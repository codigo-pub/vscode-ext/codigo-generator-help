
 - [English](https://gitlab.com/codigo-pub/vscode-ext/codigo-generator-help/-/blob/master/README/README.md?ref_type=heads)
 - [Français](https://gitlab.com/codigo-pub/vscode-ext/codigo-generator-help/-/blob/master/README/README.fr.md?ref_type=heads)
 - [Português](https://gitlab.com/codigo-pub/vscode-ext/codigo-generator-help/-/blob/master/README/README.pt.md?ref_type=heads)
 - [Español](https://gitlab.com/codigo-pub/vscode-ext/codigo-generator-help/-/blob/master/README/README.es.md?ref_type=heads)
 - [Italiano](https://gitlab.com/codigo-pub/vscode-ext/codigo-generator-help/-/blob/master/README/README.it.md?ref_type=heads)
 - [Deutsch](https://gitlab.com/codigo-pub/vscode-ext/codigo-generator-help/-/blob/master/README/README.de.md?ref_type=heads)
 - [Русский](https://gitlab.com/codigo-pub/vscode-ext/codigo-generator-help/-/blob/master/README/README.ru.md?ref_type=heads)

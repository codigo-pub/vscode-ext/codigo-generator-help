# codigo-generator

<p align="center">
    <img src="https://twini.fra1.cdn.digitaloceanspaces.com/vscode-ext/img/generator/img_codigo_generator_001.png" alt="codigo-generator" width="50%">
</p>

<p align="center">
    <img src="https://twini.fra1.cdn.digitaloceanspaces.com/vscode-ext/img/generator/mistral.png" alt="codigo-generator" width="100px">
    <img src="https://twini.fra1.cdn.digitaloceanspaces.com/vscode-ext/img/generator/openai.png" alt="codigo-generator" width="100px">
    <img src="https://twini.fra1.cdn.digitaloceanspaces.com/vscode-ext/img/generator/deepseek.png" alt="codigo-generator" width="100px">
</p>

> 🚀 Codigo Generator – Was ist das?  
- **Codigo Generator** automatisiert die Erstellung von Basisdateien für deine Projekte, indem er traditionelle Code-Generierung mit KI kombiniert.  
- Es hilft dir, schnell die Struktur deines Projekts zu erstellen, sodass du dich auf die Entwicklung der Funktionen konzentrieren kannst, anstatt auf wiederholte Setups.  
- Besonders nützlich beim Starten eines **neuen Projekts** oder beim Hinzufügen einer **neuen Funktion** (mit neuen Tabellen zum Beispiel).  

> Wie funktioniert es?  
- Es nimmt ein **Datenbankschema** und generiert mehrere Dateien je nach deinen Bedürfnissen.  
- Wenn du möchtest, kann **Codigo Generator** diese Dateien auch mit KI verbessern, um noch mehr Automatisierung zu bieten!  

> Welche Arten von Dateien werden generiert?  
- Hauptsächlich **Modelle**, **Repositories**, **Services**, **Controllers** und weitere Dateien, abhängig von deinem Technologie-Stack.  

> Welche Technologien werden unterstützt?  
- **TypeScript**, **PHP**, **Java**, **Python**, **C#**… und hoffentlich noch viele mehr!  

> Frameworks?  
- Ja! Sieh dir die Liste unten für unterstützte Frameworks und Integrationen an.  


# Demo

> Demo auf YouTube

[![Watch on youtube](https://img.youtube.com/vi/x60cT2zaeiA/maxresdefault.jpg)](https://www.youtube.com/watch?v=x60cT2zaeiA&list=PLEjhpI6uFRac0XlOMNjbN8WYVkDLP2zcE)

> Demo mit php-Template und symfony-Prozessoren

![wie man es benutzt](https://twini.fra1.cdn.digitaloceanspaces.com/vscode-ext/img/codigo-generator-php-symfony-demo.gif)


# Anforderungen
Sie benötigen mindestens:

- Eine `.env`-Datei im Stammordner Ihres Projekts, da **codigo-generator** Umgebungsvariablen über **dotenv** und diese Datei lädt. Wir erwarten, dass in dieser Datei geheime Daten enthalten sind. Zum Beispiel:

```
MYSQL_DB1_ROOT_PASSWORD="OnceUponAMidnightDreary"
MYSQL_DB2_ROOT_PASSWORD="WhileIPondered"
```

- Eine JSON-Konfigurationsdatei. In dieser Datei geben Sie an, welchen Schlüssel Sie für das Passwort verwenden möchten.

Beispiel:

```JSON
{
    "global_config": {
        "db_type": "mysql",
        "database": {
            "host": "localhost",
            "user": "quizuser",
            "password": "${MYSQL_DB1_ROOT_PASSWORD}",
            "database": "quiz", 
            "port": 3306
        },
        "useAI": {
            "enabled": true,
            "apiKey": "${CHATGPT_APIKEY}"
        },        
        "template": "php",
        "packageBaseName": "MyApp",
        "out_folder": "./out"
    },
    "processors": [
        { "name": "doctrine_repository" },
        { "name": "doctrine_entity" }
    ]
}
```

**Hinweis**: Die Verwendung dieser Methode ist nicht obligatorisch, Sie könnten ein einfaches Passwort verwenden, aber wir empfehlen dringend die Verwendung einer .env-Datei als gute Praxis. So können Sie die Konfigurationsdatei bei Bedarf committen.


# Konfigurationsdatei

## Beispiele zur Konfiguration

- [Klicken Sie hier, um einige Beispiele zu sehen](https://gitlab.com/codigo-pub/vscode-ext/codigo-generator-help/-/tree/master/examples?ref_type=heads)

## Globale Konfiguration
Der Schlüssel **global_config** enthält die globale Konfiguration.

### db_type
**db_type** definiert den Datenbanktyp, er kann folgende Werte haben:
- mysql
- mariadb
- postgresql

### database
**database** enthält Datenbankinformationen zur Verbindung mit der Datenbank. Sie müssen diese Informationen bereitstellen:
- host
- user
- password
- port
- schema [optional] definiert den zu verwendenden Schemanamen. Standardmäßig "public". Wird in mysql/mariaDB-Datenbanken nicht verwendet.
- many_to_many_sep [optional] definiert den Zeichenfolgen-Trenner zwischen beiden Tabellen in einer ManyToMany-Pivot-Tabelle. "\_has\_" als Standard.

### useAI

**useAI** ist optional. Es enthält Informationen zur Nutzung eines KI-Dienstes, um die Vervollständigung von Dateien zu verbessern, die mit **codigo-generator** erstellt wurden. Beachte, dass **codigo-generator** sehr schnell ist, aber die Verwendung von KI die Verarbeitungszeit erheblich verlängern kann. Der Mehrwert macht dies jedoch oft lohnenswert. Überprüfe die KI-Spalte in der Prozessorliste, um festzustellen, ob ein Prozessor KI verwenden kann.

Achtung: Wenn du KI verwendest, musst du möglicherweise einige Änderungen am Code vornehmen. Das ist völlig normal.

Wenn du KI verwenden möchtest, musst du die folgenden Informationen angeben:
- **apiKey**: Dein API-Schlüssel für den KI-Dienst. Es wird dringend empfohlen, einen Platzhalter zu verwenden (z. B. `${CHATGPT_APIKEY}`) und diesen Schlüssel in einer Umgebungsdatei zu speichern!
- **enabled**: [Optional] Ein boolescher Wert zur Aktivierung oder Deaktivierung der KI. Der Standardwert ist `false`.
- **aiName**: [Optional] Der Name des zu verwendenden KI-Dienstes. Standardmäßig wird der OpenAI-Dienst verwendet. Folgende Werte sind für **aiName** möglich:
  - **OpenAI**: OpenAI-API-Dienst
  - **DeepSeek**: DeepSeek-API-Dienst
  - **Mistral** : Mistral-API-Dienst
- **duration_warning**: [Optional] Zeitlimit in Sekunden (z. B. `"duration_warning": 20`), bevor eine Warnung für einen zu langen KI-Prozess ausgelöst wird. Die Warnung gibt die verstrichene Zeit an und schlägt eine Anpassung der Einstellungen vor.


#### Zusätzliche Eigenschaften
Diese zusätzlichen Eigenschaften sind optional. Sei vorsichtig bei ihrer Verwendung, da sie die Verarbeitungszeit weiter erhöhen können.

- **model**: Der Name des KI-Modells für den gewählten KI-Dienst. Falls nicht angegeben, wird ein Standardwert verwendet, der vom jeweiligen KI-Dienst abhängt:
  - OpenAI: **gpt-4o**
  - DeepSeek: **deepseek-chat**
  - Mistral : **ministral-8b-latest**
- **temperature**: Definiert die *Temperatur* für KI-Anfragen. Wird nicht gesendet, wenn sie nicht angegeben ist.
- **max_completion_tokens**: Legt die maximale Anzahl von Tokens für KI-Ergebnisse fest. Wird nicht gesendet, wenn sie nicht angegeben ist.

Hier sind einige Konfigurationsbeispiele:

```JSON
"useAI": {
    "enabled": true,
    "model": "gpt-4o-mini",
    "apiKey": "${CHATGPT_APIKEY}"
}
```

```JSON
"useAI": {
    "enabled": true,
    "aiName": "DeepSeek",
    "apiKey": "${DEEPSEEK_APIKEY}"
}
```

```JSON
"useAI": {
    "enabled": true,
    "aiName": "Mistral",
    "model": "mistral-small-latest",
    "apiKey": "${MISTRAL_APIKEY}"
}
```


### template
**template** definiert hauptsächlich die Sprache (nun, bisher), es kann folgende Werte haben:
- typescript
- php
- java
- python
- csharp

### packageBaseName
**packageBaseName** ist optional, aber **notwendig für einige Templates**, da dies Ihr Basis-Paket/Namensraum-Name ist.

Wenn Sie den **model**-Prozessor mit der **php**-Vorlage und `"packageBaseName": "MyApp"` verwenden, enthalten Ihre *model*-Dateien `namespace MyApp\Models;`.

Wenn Sie den **model**-Prozessor mit der **java**-Vorlage und `"packageBaseName": "com.myapp"` verwenden, enthalten Ihre *model*-Dateien `package com.myapp.models;`.

Wenn Sie den **model**-Prozessor mit der **csharp**-Vorlage und `"packageBaseName": "AngularApp1.Server"` verwenden, enthalten Ihre *model*-Dateien `namespace AngularApp1.Server.Models;`.

Django-Prozessoren benötigen `packageBaseName`, dies ist der Name der App.

### out_folder
**out_folder** ist der Ort, an dem die Dateien erstellt werden müssen. Codigo-generator generiert Dateien basierend auf Ihrer Konfigurationsdatei, also ja, es werden Dateien generiert und sie werden dort sein. Dieser Pfadwert kann sein:

- ein **relativer Pfad zu diesem Arbeitsbereich**, Ihr relativer Pfad **muss mit einem Punkt beginnen**, z.B. => `"out_folder": "./out"`
- ein **absoluter Pfad** => `"out_folder": "C:\\Users\\marco\\OneDrive\\tmp\\out"`

### skip_these_tables
**skip_these_tables** ist optional. Es ist ein Array von **Tabellennamen, die übersprungen werden sollen**.

Beispiel: `"skip_these_tables": [ "classrooms" ]`

Dieses Beispiel führt dazu, dass die **classrooms**-Tabelle im Prozess übersprungen wird.

### only_these_tables
**only_these_tables** ist optional. Es ist eine Tabelle mit Tabellennamen, die **ausschließlich** bearbeitet werden.

Beispiel: `"only_these_tables": [ "classrooms", "users" ]`

Dieses Beispiel bewirkt, dass die Tabellen **classrooms** und **users** bearbeitet werden, aber nicht die anderen. Wenn **classrooms** auch in der Tabelle **skip_these_tables** enthalten wäre, würde sie ignoriert (fair, oder?).

## Prozessoren
Der Schlüssel **processors** enthält ein Array von Prozessoren. Jeder Prozessor hat eine Aufgabe... für Sie.

Jeder Prozessor muss diese Informationen haben:
- **name**: der Name des Prozessors (siehe unten für eine Liste der verfügbaren Prozessornamen)
- ***generate***: ist optional. Es ist ein boolescher Wert, **true** bedeutet, dass der Prozessor arbeitet, **false**, dass er übersprungen wird. Standardmäßig auf true.
- ***parameter_prefix***: ist optional. Dies ist der Text, der an die Funktionsparameter angehängt wird. Standardmäßig ist es 'r'.
  - Beispiel: `function sayHello(rName: string) { ... }`
  - mit dieser Konfiguration `"parameter_prefix": "_"` haben wir: `function sayHello(_Name: string) { ... }`
- ***custom_base_template***: ist optional. Es ist der absolute Pfad Ihrer benutzerdefinierten Vorlagendatei, wenn Sie etwas Besonderes wünschen. Sie sollten dies nicht verwenden, wenn es nicht notwendig ist. Weitere Informationen finden Sie unten.

- ***extra_config***: ist optional. Es ist ein JSON mit benutzerdefinierten Konfigurationen, die nur für einige Prozessoren funktionieren. Weitere Informationen finden Sie unten.

- ***useCamelCaseForClass*** : ist optional. true, um camelCase für Klassennamen zu verwenden, false andernfalls. Der Standardwert ist true.
- ***useCamelCaseForField*** : ist optional. true, um camelCase für Felderamen zu verwenden, false andernfalls. Der Standardwert ist true. Beachten Sie, dass einige Prozessoren diese Eigenschaft aufgrund der Sprachnormalisierung ignorieren können.

### Prozessorliste
Hier ist eine Liste der verfügbaren Prozessoren für jedes Template. Hoffentlich wird sie im Laufe der Zeit wachsen.

Einige Prozessoren können zusätzliche Dateien generieren.

#### typescript-Template

| Name                          | Details                                                                                               | Zusätzliche Dateien                   | AI ?  |
| ----------------------------- | ----------------------------------------------------------------------------------------------------- |-------------------------------------- | ----- |
| model                         | Modelklasse                                                                                           |                                       |       |
| angular_service               | Angular Service-Klasse zum Aufrufen der Back-End-API                                                  | 'abstract_api.service.ts'             | ja    |
| row_mapper                    | Rowmapper-Klasse, ähnlich einer Modelklasse, jedoch nur mit Beziehungen zu untergeordneten Klassen    |                                       |       |
| loopback_model                | Loopback-Framework-Klassenmodell                                                                      |                                       |       |
| loopback_repository           | Repository-Klasse des Loopback-Frameworks                                                             |                                       |       |
| loopback_service              | Service-Klasse des Loopback-Frameworks                                                                |                                       | ja    |
| loopback_controller           | Controller-Klasse des Loopback-Frameworks                                                             |                                       | ja    |
| postman                       | Postman-Sammlung (wird im Stammverzeichnis sein)                                                      |                                       |       |
| angular_view_model_form       | Angular-Formularansichtskomponente für das Modell                                                     | angular_generic_model_selector-Komponente | ja |
| angular_view_model_card       | Angular-Kartenansichtskomponente für das Modell                                                       |                                       | ja |
| angular_view_model_list       | Angular-Listenansichtskomponente für das Modell                                                       | 'angular_confirm_dialog'-Komponente     | ja |
| angular_view_model_list_page  | Angular-Seite zur Anzeige der Listenansichtskomponente                                                |                                       |       |
| ionic_angular_view_model_form       | Ionic-Angular-Formularansichtskomponente für das Modell                                                     | angular_generic_model_selector-Komponente | ja |
| ionic_angular_view_model_card       | Ionic-Angular-Kartenansichtskomponente für das Modell                                                       |                                       | ja |
| ionic_angular_view_model_list       | Ionic-Angular-Listenansichtskomponente für das Modell                                                       | 'angular_confirm_dialog'-Komponente     | ja |
| ionic_angular_view_model_list_page  | Ionic-Angular-Seite zur Anzeige der Listenansichtskomponente                                                |                                       |       |

#### php-Template

| Name                      | Details                                                                   | Zusätzliche Dateien                   | AI ?  |
| ------------------------- | ------------------------------------------------------------------------- |-------------------------------------- | ----- |
| model                     | Modelklasse                                                               |                                       |       |
| pdo_dao                   | DAO (Repository)-Klasse mit PDO                                           |                                       |       |
| doctrine_entity           | Doctrine-Entity für Symfony-Benutzer (php 8)                              |                                       |       |
| doctrine_repository       | Doctrine-Repository für Symfony-Benutzer (php 8)                          |                                       |       |
| artisan_make_model        | Generieren Sie eine einzelne Bash-Datei mit Artisan-Make-Befehlen         |                                       |       |
| eloquent_model            | Eloquent-Modell-Datei für Laravel-Benutzer                                |                                       |       |
| symfony_service           | Symfony-Dienst                                                            |                                       |       |
| symfony_api_controller    | Symfony-api-Controller                                                    |                                       |       |
| postman                   | Postman-Sammlung (wird im Stammverzeichnis sein)                          |                                       |       |

#### java-Template

| Name                      | Details                                                                   | Zusätzliche Dateien                   | AI ?  |
| ------------------------- | ------------------------------------------------------------------------- |-------------------------------------- | ----- |
| model                     | Modelklasse                                                               |                                       |       |
| jpa_entity                | Spring JPA-Entity                                                         |                                       |       |
| jpa_repository            | Spring JPA-Repository                                                     |                                       | ja    |
| spring_service            | Spring-Dienst unter Verwendung des Repository                             |                                       | ja    |
| spring_controller         | Spring-Controller unter Verwendung des Service                            |                                       | ja    |
| postman                   | Postman-Sammlung (wird im Stammverzeichnis sein)                          |                                       |       |

#### Python Vorlage

| Name                      | Details                                                                                   | Zusätzliche Dateien                   | AI ?  |
| ------------------------- | ----------------------------------------------------------------------------------------- | ------------------------------------- | ----- |
| django_model              | Django-Modellklasse                                                                       |                                       |       |
| django_import_models      | Eine `models.py`-Datei zum Importieren Ihrer Modelle, wird im Stammverzeichnis sein       |                                       |       |
| django_controller         | Django-Controller-Klasse                                                                  |                                       |       |
| django_controller_path    | URL-Muster des Django-Controllers                                                         |                                       |       |
| postman                   | Postman-Sammlung (wird im Stammverzeichnis sein)                                          |                                       |       |

#### csharp-Vorlage

| Name                      | Details                                                                   | Zusätzliche Dateien                   | AI ?  |
| ------------------------- | ------------------------------------------------------------------------- |-------------------------------------- | ----- |
| model                     | Modellklasse                                                              |                                       |       |
| efcore_model              | EF Core-Modellklasse (vorzugsweise efcore zur Modellerstellung verwenden) |                                       |       |
| efcore_repository         | EF Core-Repository                                                        | IAbstractRepository/AbstractRepository|       |
| net_service               | .Net-Dienst                                                               |                                       |       |
| net_controller            | .Net-Controller-Klasse                                                    |                                       |       |
| net_program               | .Net Program.cs-Datei mit Import für Dienste/Repositories                 |                                       |       |
| postman                   | Postman-Sammlung (wird im Stammverzeichnis sein)                          |                                       |       |

# custom_base_template
***custom_base_template*** ist der absolute Pfad zu Ihrem benutzerdefinierten Vorlagenverzeichnis, wenn Sie etwas Besonderes möchten.

> Sie sollten dies nur verwenden, wenn es notwendig ist.

**Prozessoren verwenden Vorlagendateien, um Daten zu füllen**. Jede Vorlage erhält eingehende Daten, die durch **{{ }}** begrenzt sind. Wenn Sie Ihre eigene verwenden, müssen Sie wissen, was sie erhalten wird.

Sie können die Vorlagen im `templates` Ordner im GitLab-Repository überprüfen.

**Beispiel**:

Angenommen, Sie möchten Ihre eigene Vorlagendatei für die `java` Vorlage und den `model` Prozessor. Erstellen Sie zuerst einen lokalen Ordner, um Ihre Vorlagendatei zu speichern. Zum Beispiel: "c:/my_templates". Gehen Sie dann zum [Ordner `templates` im GitLab-Projektrepository](https://gitlab.com/codigo-pub/vscode-ext/codigo-generator/-/tree/master/templates?ref_type=heads), dann in den Ordner `java`.

Dort finden Sie zwei Dateien, die sich auf den `model` Prozessor beziehen (es könnten eine oder mehrere sein):

- tpl_model_enum.java
- tpl_model.java

Laden Sie diese beiden Dateien in Ihren Ordner "c:/my_templates", aber **benennen Sie sie nicht um**!

Ändern Sie ihren Inhalt nach Ihren Bedürfnissen. Achten Sie auf die Daten **{{ }}**.

Dann sind Sie startklar. Sie können Ihren Prozessor ausführen und er wird Ihre Dateien verwenden.

# extra_config
*extra_config* kann, wie bereits erwähnt, nur mit einigen Prozessoren verwendet werden.

Beispiel mit einer zusätzlichen Konfiguration:

```JSON
    "processors": [
        { "name": "model", "generate": false },
        { "name": "jpa_entity", "extra_config": { "lombok": false } },
        { "name": "jpa_repository", "generate": false }
    ]
```

Dies ist die Liste der Prozessoren und ihre verfügbaren extra_config:

| Vorlage        | Name                      | extra_config_eigenschaft | Standardwert        | Details                                                 | Beispiel                                          |
| -------------- | ------------------------- | ------------------------ | ------------------- | ------------------------------------------------------- | ------------------------------------------------- |
| java           | model                     | lombok                   | true                | Lombok-Anmerkungen hinzufügen                           | "extra_config": { "lombok": false }               |
| java           | jpa_entity                | lombok                   | true                | Lombok-Anmerkungen hinzufügen                           | "extra_config": { "lombok": false }               |
| java           | jpa_entity                | use_enum                 | true                | Verwenden Sie die Enum-Klasse, wenn enum                | "extra_config": { "use_enum": false }             |
| java           | spring_service            | logger                   | true                | Logger hinzufügen                                       | "extra_config": { "logger": false }               |
| java           | spring_controller         | logger                   | true                | Logger hinzufügen                                       | "extra_config": { "logger": false }               |
| typescript     | loopback_repository       | datasource_name          | _schema name_       | Datenquellenname für das Repository                     | "extra_config": { "datasource_name": "MyApp" }    |
| php            | artisan_make_model        | option                   | *empty string*      | Artisan Modellbefehl Option                             | "extra_config": { "option": "--all" }             |
| php            | eloquent_model            | do_getters_setters       | false               | Getter/Setter hinzufügen                                | "extra_config": { "do_getters_setters": true }    |
| php            | symfony_api_controller    | base_api                 | "/api"              | Basis-URL für die API                                   | "extra_config": { "base_api": "/myapi" }          |
| php            | symfony_api_controller    | folder_api               | "Api"               | Ordnername, in den die Dateien gelegt werden sollen     | "extra_config": { "folder_api": "MyApi" }         |
| java           | spring_security_jwt       | user_field/pass_field/role_field  |            | Du musst die Felder benennen, die verwendet werden, um den Benutzer zu identifizieren (E-Mail, Benutzername, ..), das Passwort (pass, password, ...) und die Rolle (rollenname, role, ...) | "extra_config": { "user_field": "users.email", "pass_field": "users.password", "role_field": "roles.role" } |

## postman processors

Dies sind die `extra_config`-Eigenschaften, die für `postman`-Prozessoren verfügbar sind.
**with_base_api** ermöglicht es Ihnen zu definieren, ob Ihre API einen Haupt-Basisnamen hat. Zum Beispiel `/api`. Als beste Praxis sollte dies als Umgebungsvariable `{{base_api}}` in Ihrer Postman-Sammlung hinzugefügt werden.

``` "extra_config": { "with_base_api": false } ```

**name** ermöglicht es Ihnen, den Dateinamen der von dem Prozessor generierten Postman-Datei festzulegen. Standardmäßig ist es `default-postman`. Für Java kann es den Paketnamen enthalten.

``` "extra_config": { "name": "myproject-postman" } ```

## use_model_for_api_endpoint
**use_model_for_api_endpoint** ist eine `extra_config`-Eigenschaft, die für Prozessoren verfügbar ist, die Dateien mit API-Pfaden erstellen (z. B. Postman, Controller, Angular-Services usw.).
Der Standardwert ist `false`.
Verhalten:

 - true : Der Basismodellpfad verwendet den Tabellennamen. Zum Beispiel: /users
 - false : Der Basismodellpfad verwendet den Modellnamen. Zum Beispiel: /user

## standalone für Angular
**standalone** ist eine `extra_config`-Eigenschaft, die für Angular-Prozessoren verfügbar ist, um die Erstellung von Standalone-Komponenten anzufordern. Der Standardwert ist `true`.

# Erweiterungseinstellungen

Derzeit keine Einstellungen für die Erweiterung.

---

# Autor

Marc Flausino

# Mitwirkende

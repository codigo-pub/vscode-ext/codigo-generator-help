# codigo-generator

<p align="center">
    <img src="https://twini.fra1.cdn.digitaloceanspaces.com/vscode-ext/img/generator/img_codigo_generator_001.png" alt="codigo-generator" width="50%">
</p>

<p align="center">
    <img src="https://twini.fra1.cdn.digitaloceanspaces.com/vscode-ext/img/generator/mistral.png" alt="codigo-generator" width="100px">
    <img src="https://twini.fra1.cdn.digitaloceanspaces.com/vscode-ext/img/generator/openai.png" alt="codigo-generator" width="100px">
    <img src="https://twini.fra1.cdn.digitaloceanspaces.com/vscode-ext/img/generator/deepseek.png" alt="codigo-generator" width="100px">
</p>

> 🚀 Codigo Generator – ¿Qué es?  
- **Codigo Generator** automatiza la creación de archivos base para tus proyectos, combinando generación de código tradicional con IA.  
- Te ayuda a configurar rápidamente la estructura de tu proyecto, permitiéndote concentrarte en desarrollar las funcionalidades en lugar de la configuración repetitiva.  
- Especialmente útil cuando empiezas un **nuevo proyecto** o agregas una **nueva funcionalidad** (con nuevas tablas, por ejemplo).  

> ¿Cómo funciona?  
- Toma un **esquema de base de datos** y genera varios archivos según tus necesidades.  
- Si lo deseas, **Codigo Generator** también puede mejorar esos archivos utilizando IA para aún más automatización.  

> ¿Qué tipo de archivos genera?  
- Principalmente **modelos**, **repositories**, **services**, **controllers**, y otros archivos, dependiendo de tu stack tecnológico.  

> ¿Qué tecnologías son compatibles?  
- **TypeScript**, **PHP**, **Java**, **Python**, **C#**… ¡y espero que muchas más en el futuro!  

> ¿Frameworks?  
- ¡Sí! Consulta la lista a continuación para los frameworks e integraciones compatibles.  


# Demo

> Demo en youtube

[![Watch on youtube](https://img.youtube.com/vi/x60cT2zaeiA/maxresdefault.jpg)](https://www.youtube.com/watch?v=x60cT2zaeiA&list=PLEjhpI6uFRac0XlOMNjbN8WYVkDLP2zcE)


> Demo con plantilla php y procesadores symfony

![cómo usarlo](https://twini.fra1.cdn.digitaloceanspaces.com/vscode-ext/img/codigo-generator-php-symfony-demo.gif)


# Requisitos
Necesitarás al menos:

- Un archivo `.env` en la carpeta raíz de tu proyecto, ya que **codigo-generator** cargará las variables de entorno a través de **dotenv** y este archivo. Esperamos tener datos secretos en este archivo. Por ejemplo:

```
MYSQL_DB1_ROOT_PASSWORD="OnceUponAMidnightDreary"
MYSQL_DB2_ROOT_PASSWORD="WhileIPondered"
```

- Un archivo de configuración json. Indicarás en este archivo qué clave quieres usar para la contraseña.

Ejemplo:

```JSON
{
    "global_config": {
        "db_type": "mysql",
        "database": {
            "host": "localhost",
            "user": "quizuser",
            "password": "${MYSQL_DB1_ROOT_PASSWORD}",
            "database": "quiz",
            "port": 3306
        },
        "useAI": {
            "enabled": true,
            "apiKey": "${CHATGPT_APIKEY}"
        },        
        "template": "php",
        "packageBaseName": "MyApp",
        "out_folder": "./out"
    },
    "processors": [
        { "name": "doctrine_repository" },
        { "name": "doctrine_entity" },
        { "name": "pdo_dao" }
    ]
}
```

**Nota**: No es obligatorio usar este método, podrías usar una contraseña simple, pero recomendamos encarecidamente el uso de un archivo `.env` como buena práctica. Así, podrás comprometer el archivo de configuración si es necesario.

# Archivo de Configuración

## Ejemplos de configuración

 - [Click here to see some examples](https://gitlab.com/codigo-pub/vscode-ext/codigo-generator-help/-/tree/master/examples?ref_type=heads)

## Configuración Global
La clave **global_config** contiene la configuración global.

### db_type
**db_type** define el tipo de base de datos, puede tener los siguientes valores:
- mysql
- mariadb
- postgresql

### database
**database** contiene información de la base de datos para conectarse a la base de datos. Debes proporcionar esta información:
- host
- user
- password
- port
- schema [opcional]: define el nombre del esquema a utilizar. El valor predeterminado es public. No utilizado en bases de datos mysql/mariaDB.
- many_to_many_sep [opcional] define el separador de cadena entre ambas tablas en una tabla pivote ManyToMany. "\_has\_" por defecto.

### useAI

**useAI** es opcional. Contiene información para utilizar un servicio de IA y mejorar la finalización de los archivos generados por **codigo-generator**. Ten en cuenta que, aunque codigo-generator es muy rápido, el uso de IA puede aumentar significativamente el tiempo de procesamiento. Sin embargo, el valor añadido suele compensarlo. Consulta la columna de IA en la lista de procesadores para verificar si un procesador puede usar IA.

Atención, al utilizar IA, es posible que necesites realizar algunos cambios en el código. Eso es normal.

Si deseas utilizar IA, debes proporcionar la siguiente información:
- **apiKey**: Tu clave API del servicio de IA. Se recomienda encarecidamente utilizar un marcador de posición (ej. `${CHATGPT_APIKEY}`) y almacenar esta clave en un archivo de entorno.
- **enabled**: [Opcional] Un valor booleano para activar o desactivar la IA. El valor predeterminado es `false`.
- **aiName**: [Opcional] El nombre del servicio de IA a utilizar. Por defecto, usamos el servicio OpenAI. Estos son los valores posibles para **aiName**:
  - **OpenAI**: Servicio API de OpenAI
  - **DeepSeek**: Servicio API de DeepSeek
  - **Mistral** : Servicio API de Mistral
- **duration_warning**: [Opcional] Límite de tiempo en segundos (por ej. `"duration_warning": 20`) antes de activar una advertencia para un proceso de IA que tarda demasiado. La advertencia indica el tiempo transcurrido y sugiere ajustar la configuración.


#### Propiedades adicionales
Estas propiedades adicionales son opcionales. Úsalas con precaución, ya que pueden aumentar aún más el tiempo de procesamiento.

- **model**: El nombre del modelo de IA para tu servicio de IA. Si no se especifica, se usará un valor predeterminado según el servicio elegido:
  - OpenAI: **gpt-4o**
  - DeepSeek: **deepseek-chat**
  - Mistral : **ministral-8b-latest**
- **temperature**: Define la *temperatura* para las solicitudes de IA. No se enviará si no se proporciona.
- **max_completion_tokens**: Establece el número máximo de tokens para las respuestas de IA. No se enviará si no se proporciona.

Aquí tienes algunos ejemplos de configuración:

```JSON
"useAI": {
    "enabled": true,
    "model": "gpt-4o-mini",
    "apiKey": "${CHATGPT_APIKEY}"
}
```

```JSON
"useAI": {
    "enabled": true,
    "aiName": "DeepSeek",
    "apiKey": "${DEEPSEEK_APIKEY}"
}
```

```JSON
"useAI": {
    "enabled": true,
    "aiName": "Mistral",
    "model": "mistral-small-latest",
    "apiKey": "${MISTRAL_APIKEY}"
}
```

### template
**template** define principalmente el lenguaje (hasta ahora), puede tener los siguientes valores:
- typescript
- php
- java
- python
- csharp

### packageBaseName
**packageBaseName** es opcional, pero **necesario para algunas plantillas**, ya que es el nombre del paquete/espacio de nombres base.

Si utiliza el procesador de **model** con la plantilla de **php** y `"packageBaseName": "MyApp"`, sus archivos de *model* contendrán `namespace MyApp\Models;`.

Si utiliza el procesador de **model** con la plantilla de **java** y `"packageBaseName": "com.myapp"`, sus archivos de *model* contendrán `package com.myapp.models;`.

Si utiliza el procesador de **model** con la plantilla de **csharp** y `"packageBaseName": "AngularApp1.Server"`, sus archivos de *model* contendrán `namespace AngularApp1.Server.Models;`.

Los procesadores de Django necesitan `packageBaseName`, que es el nombre de la aplicación.

### out_folder
**out_folder** es donde deben crearse los archivos. Codigo-generator genera archivos en función de tu archivo de configuración, así que sí, se generarán archivos y estarán allí. Este valor de ruta puede ser:

- una **ruta relativa a este espacio de trabajo**, tu ruta relativa **debe comenzar con un punto**, por ejemplo => `"out_folder": "./out"`
- una **ruta absoluta** => `"out_folder": "C:\\Users\\marco\\OneDrive\\tmp\\out"`

### skip_these_tables
**skip_these_tables** es opcional. Es un array de **nombres de tabla para omitir**.

Ejemplo: `"skip_these_tables": [ "classrooms" ]`

Este ejemplo hará que la tabla **classrooms** sea omitida en el proceso.

### only_these_tables
**only_these_tables** es opcional. Es una tabla de nombres de tablas que serán tratadas **exclusivamente**.

Ejemplo: `"only_these_tables": [ "classrooms", "users" ]`

Este ejemplo hará que las tablas **classrooms** y **users** sean tratadas, pero no las demás. Si **classrooms** también estuviera en la tabla **skip_these_tables**, sería ignorada (justo, ¿verdad?).

## Procesadores
La clave **processors** contiene una matriz de procesadores. Cada procesador tiene un trabajo que hacer... para ti.

Cada procesador debe tener esta información:
- **name**: el nombre del procesador (ver abajo para una lista de los nombres de procesador disponibles)
- ***generate***: es opcional. Es un valor booleano, **true** significa que el procesador funcionará, **false** será omitido. Por defecto, true.
- ***parameter_prefix***: es opcional. Este es el texto que se añadirá a los parámetros de la función. Por defecto, es 'r'.
  - Ejemplo: `function sayHello(rName: string) { ... }`
  - con esta configuración `"parameter_prefix": "_"`, tendremos: `function sayHello(_Name: string) { ... }`
- ***custom_base_template***: es opcional. Es la ruta absoluta de tu archivo de plantilla personalizada si quieres algo especial. No debes usar esto si no es necesario. Ver abajo para más información.

- ***extra_config***: es opcional. Es un json con configuración personalizada que funcionará solo para algunos procesadores. Ver abajo para más información.

- ***useCamelCaseForClass*** : es opcional. true para usar camelCase en los nombres de las clases, false en caso contrario. El valor predeterminado es true.
- ***useCamelCaseForField*** : es opcional. true para usar camelCase en los nombres de los campos, false en caso contrario. El valor predeterminado es true. Tenga en cuenta que algunos procesadores pueden ignorar esta propiedad debido a la normalización del idioma.


### Lista de Procesadores
Aquí tienes una lista de procesadores disponibles para cada plantilla. ¡Esperamos que crezca con el tiempo!

Algunos procesadores pueden generar archivos adicionales.

#### Plantilla typescript

| nombre de processor           | detalles                                                                  | archivos adicionales                  | AI ?  |
| ----------------------------- | ------------------------------------------------------------------------- |-------------------------------------- | ----- |
| model                         | Clase de modelo                                                           |                                       |       |
| angular_service               | Clase de servicio Angular para llamadas al backend de la API              | 'abstract_api.service.ts'             | sí    |
| loopback_model                | Modelo de clase del framework loopback                                    |                                       |       |
| loopback_repository           | repositorio de clase del framework loopback                               |                                       |       |
| loopback_service              | servicio de clase del framework loopback                                  |                                       | sí    |
| loopback_controller           | controlador de clase del framework loopback                               |                                       | sí    |
| postman                       | Colección de Postman (estará en la raíz)                                  |                                       |       |
| angular_view_model_form       | Componente de vista de formulario Angular para el modelo                  | componente angular_generic_model_selector | sí |
| angular_view_model_card       | Componente de vista de tarjeta Angular para el modelo                     |                                       | sí    |
| angular_view_model_list       | Componente de vista de lista Angular para el modelo                       | componente 'angular_confirm_dialog'   | sí    |
| angular_view_model_list_page  | Página Angular para mostrar el componente de vista de lista               |                                       |       |
| ionic_angular_view_model_form       | Componente de vista de formulario Ionic Angular para el modelo                  | componente angular_generic_model_selector | sí |
| ionic_angular_view_model_card       | Componente de vista de tarjeta Ionic Angular para el modelo                     |                                       | sí    |
| ionic_angular_view_model_list       | Componente de vista de lista Ionic Angular para el modelo                       | componente 'angular_confirm_dialog'   | sí    |
| ionic_angular_view_model_list_page  | Página Ionic Angular para mostrar el componente de vista de lista               |                                       |       |


#### Plantilla php

| nombre de processor       | detalles                                                                  | archivos adicionales                  | AI ?  |
| ------------------------- | ------------------------------------------------------------------------- |-------------------------------------- | ----- |
| model                     | Clase de modelo                                                           |                                       |       |
| pdo_dao                   | Clase Dao (repositorio) con PDO                                           |                                       |       |
| doctrine_entity           | Entidad Doctrine para usuarios Symfony                                    |                                       |       |
| doctrine_repository       | Repositorio Doctrine para usuarios Symfony                                |                                       |       |
| artisan_make_model        | Genera un único archivo bash con los comandos de creación del artisan     |                                       |       |
| eloquent_model            | Archivo de modelo Eloquent para usuarios de Laravel                       |                                       |       |
| symfony_service           | Servicio Symfony                                                          |                                       |       |
| symfony_api_controller    | Controlador api Symfony                                                   |                                       |       |
| postman                   | Colección de Postman (estará en la raíz)                                  |                                       |       |

#### Plantilla java

| nombre de processor       | detalles                                                                  | archivos adicionales                  | AI ?  |
| ------------------------- | ------------------------------------------------------------------------- |-------------------------------------- | ----- |
| model                     | Clase de modelo                                                           |                                       |       |
| jpa_entity                | Entidad Spring JPA                                                        |                                       |       |
| jpa_repository            | Repositorio Spring JPA                                                    |                                       | sí    |
| spring_service            | Servicio Spring usando repositorio                                        |                                       | sí    |
| spring_controller         | Controlador Spring usando servicio                                        |                                       | sí    |
| postman                   | Colección de Postman (estará en la raíz)                                  |                                       |       |

#### Plantilla de Python

| Nombre                    | Detalles                                                                  | Archivos Extra                        | AI ?  |
| ------------------------- | ------------------------------------------------------------------------- | ------------------------------------- | ----- |
| django_model              | Clase de modelo Django                                                    |                                       |       |
| django_import_models      | Un archivo `models.py` para importar tus modelos, estará en la raíz       |                                       |       |
| django_controller         | Clase de controlador de Django                                            |                                       |       |
| django_controller_path    | Patrones de URL del controlador de Django                                 |                                       |       |
| postman                   | Colección de Postman (estará en la raíz)                                  |                                       |       |

#### plantilla de csharp

| name                      | details                                                                   | extra files                           | AI ?  |
| ------------------------- | ------------------------------------------------------------------------- |-------------------------------------- | ----- |
| model                     | clase de modelo                                                           |                                       |       |
| efcore_model              | clase de modelo EF Core (se recomienda usar efcore para generar modelos)  |                                       |       |
| efcore_repository         | repositorio EF Core                                                       | IAbstractRepository/AbstractRepository|       |
| net_service               | servicio .Net                                                             |                                       |       |
| net_controller            | clase de controlador .Net                                                 |                                       |       |
| net_program               | archivo .Net Program.cs con importación de servicios/repositorios         |                                       |       |
| postman                   | colección de Postman (estará en la raíz)                                  |                                       |       |

# custom_base_template
***custom_base_template*** es la ruta absoluta de su directorio de plantillas personalizadas si desea algo especial.

> No debe usar esto a menos que sea necesario.

**Los procesadores usan archivos de plantillas para poblar datos**. Cada plantilla recibe datos entrantes delimitados por **{{ }}**. Si usa la suya propia, debe saber qué recibirá.

Puede verificar las plantillas en la carpeta `templates` en el repositorio de GitLab.

**Ejemplo**:

Digamos que desea su propio archivo de plantilla para la plantilla `java` y el procesador `model`. Primero, cree una carpeta local para almacenar su archivo de plantilla. Por ejemplo: "c:/my_templates". Luego, vaya a la [carpeta `templates` del repositorio del proyecto GitLab](https://gitlab.com/codigo-pub/vscode-ext/codigo-generator/-/tree/master/templates?ref_type=heads), luego a la carpeta `java`.

Allí encontrará dos archivos relacionados con el procesador `model` (puede haber uno o más):

- tpl_model_enum.java
- tpl_model.java

Descargue estos dos archivos a su carpeta "c:/my_templates", pero **no los renombre**.

Cambie su contenido según sus necesidades. Tenga cuidado con los datos **{{ }}**.

Entonces, está listo para ir. Puede ejecutar su procesador y usará sus archivos.

# extra_config
*extra_config* como se mencionó anteriormente, solo se puede usar con algunos procesadores.

Ejemplo con una configuración extra:

```JSON
    "processors": [
        { "name": "model", "generate": false },
        { "name": "jpa_entity", "extra_config": { "lombok": false } },
        { "name": "jpa_repository", "generate": false }
    ]
```

Aquí tienes la lista de procesadores y sus extra_config disponibles

| plantilla      | nombre                    | propiedad_extra_config | valor predeterminado | Detalles                                         | Ejemplo                                               |
| -------------- | ------------------------- | ---------------------- | -------------------- | ------------------------------------------------ | ----------------------------------------------------- |
| java           | model                     | lombok                 | true                 | Agregar anotaciones de lombok                    | "extra_config": { "lombok": false }                   |
| java           | jpa_entity                | lombok                 | true                 | Agregar anotaciones de lombok                    | "extra_config": { "lombok": false }                   |
| java           | jpa_entity                | use_enum               | true                 | Usar clase enum cuando enum                      | "extra_config": { "use_enum": false }                 |
| java           | spring_service            | logger                 | true                 | Agregar logger                                   | "extra_config": { "logger": false }                   |
| java           | spring_controller         | logger                 | true                 | Agregar logger                                   | "extra_config": { "logger": false }                   |
| typescript     | loopback_repository       | datasource_name        | _schema name_        | Nombre de la fuente de datos para el repositorio | "extra_config": { "datasource_name": "MyApp" }        |
| php            | artisan_make_model        | option                 | *empty string*       | Opción del comando de modelo Artisan             | "extra_config": { "option": "--all" }                 |
| php            | eloquent_model            | do_getters_setters     | false                | Agregar Getters/Setters                          | "extra_config": { "do_getters_setters": true }        |
| php            | symfony_api_controller    | base_api               | "/api"               | Base para la URL de la API                       | "extra_config": { "base_api": "/myapi" }              |
| php            | symfony_api_controller    | folder_api             | "Api"                | Nombre de la carpeta donde poner los archivos    | "extra_config": { "folder_api": "MyApi" }             |
| java   | spring_security_jwt | user_field/pass_field/role_field  |     | Debes nombrar los campos utilizados para identificar al usuario (email, nombre de usuario, ..), la contraseña (pass, password, ...) y el rol (nombre_del_rol, role, ...) | "extra_config": { "user_field": "users.email", "pass_field": "users.password", "role_field": "roles.role" } |

## postman processors

Estas son las propiedades `extra_config` disponibles para los procesadores `postman`.
**with_base_api** te permite definir si tu API tiene un nombre base principal. Por ejemplo, `/api`. Como buena práctica, esto se agregarían como una variable de entorno `{{base_api}}` en tu colección de Postman.

``` "extra_config": { "with_base_api": false } ```

**name** te permite definir el nombre del archivo Postman generado por el procesador. Por defecto, es `default-postman`. Para Java, puede incluir el nombre del paquete.

``` "extra_config": { "name": "myproject-postman" } ```

## use_model_for_api_endpoint
**use_model_for_api_endpoint** es una propiedad `extra_config` disponible para los procesadores que generan archivos con rutas de API (por ejemplo, postman, controlador, servicios Angular, etc.).
Su valor predeterminado es `false`.
Comportamiento:

 - true : la ruta base del modelo usa el nombre de la tabla. Por ejemplo: /users
 - false : la ruta base del modelo usa el nombre del modelo. Por ejemplo: /user

## standalone para Angular
**standalone** es una propiedad `extra_config` disponible para los procesadores de Angular que solicitan la creación de componentes independientes. El valor predeterminado es `true`.


# Configuraciones de la Extensión

Ninguna configuración de extensión por el momento.

---


# Autor

Marc Flausino

# Contribuidores


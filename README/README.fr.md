# codigo-generator

<p align="center">
    <img src="https://twini.fra1.cdn.digitaloceanspaces.com/vscode-ext/img/generator/img_codigo_generator_001.png" alt="codigo-generator" width="50%">
</p>

<p align="center">
    <img src="https://twini.fra1.cdn.digitaloceanspaces.com/vscode-ext/img/generator/mistral.png" alt="codigo-generator" width="100px">
    <img src="https://twini.fra1.cdn.digitaloceanspaces.com/vscode-ext/img/generator/openai.png" alt="codigo-generator" width="100px">
    <img src="https://twini.fra1.cdn.digitaloceanspaces.com/vscode-ext/img/generator/deepseek.png" alt="codigo-generator" width="100px">
</p>

> 🚀 Codigo Generator – Qu'est-ce que c'est ?  
- **Codigo Generator** automatise la création de fichiers de base pour vos projets, en combinant la génération de code traditionnelle et assistée par IA.  
- Il vous aide à mettre en place rapidement la structure de votre projet, vous permettant ainsi de vous concentrer sur le développement des fonctionnalités plutôt que sur la configuration répétitive.  
- Particulièrement utile lors du démarrage d'un **nouveau projet** ou de l'ajout d'une **nouvelle fonctionnalité** (avec de nouvelles tables, par exemple).  

> Comment ça fonctionne ?  
- Il prend un **schéma de base de données** et génère plusieurs fichiers selon vos besoins.  
- Si vous le souhaitez, **Codigo Generator** peut aussi améliorer ces fichiers en utilisant l'IA pour encore plus d'automatisation !  

> Quels types de fichiers génère-t-il ?  
- Principalement des **modèles**, **repositories**, **services**, **controllers**, et d'autres fichiers, selon votre stack technologique.  

> Quelles technologies sont supportées ?  
- **TypeScript**, **PHP**, **Java**, **Python**, **C#**… et j'espère encore plus à venir !  

> Des frameworks ?  
- Oui ! Consultez la liste ci-dessous pour les frameworks et intégrations supportés.  


# Démo

> Démo sur youtube

[![Watch on youtube](https://img.youtube.com/vi/x60cT2zaeiA/maxresdefault.jpg)](https://www.youtube.com/watch?v=x60cT2zaeiA&list=PLEjhpI6uFRac0XlOMNjbN8WYVkDLP2zcE)


> Démo avec un modèle php et des processeurs symfony

![comment l'utiliser](https://twini.fra1.cdn.digitaloceanspaces.com/vscode-ext/img/codigo-generator-php-symfony-demo.gif)


# Pré-requis
Vous aurez besoin au minimum de :

- Un fichier `.env` dans le dossier racine de votre projet, car **codigo-generator** chargera les variables d'environnement via **dotenv** et ce fichier. Nous nous attendons à avoir des données secrètes dans ce fichier. Par exemple :

```
MYSQL_DB1_ROOT_PASSWORD="OnceUponAMidnightDreary"
MYSQL_DB2_ROOT_PASSWORD="WhileIPondered"
```

- Un fichier de configuration json. Vous indiquerez dans ce fichier la clé que vous souhaitez utiliser pour le mot de passe.

Exemple :

```JSON
{
    "global_config": {
        "db_type": "mysql",
        "database": {
            "host": "localhost",
            "user": "quizuser",
            "password": "${MYSQL_DB1_ROOT_PASSWORD}",
            "database": "quiz", 
            "port": 3306
        },
        "useAI": {
            "enabled": true,
            "apiKey": "${CHATGPT_APIKEY}"
        },        
        "template": "php",
        "packageBaseName": "MyApp",
        "out_folder": "./out"
    },
    "processors": [
        { "name": "doctrine_repository" },
        { "name": "doctrine_entity" }
    ]
}
```

**Remarque** : Utiliser cette méthode n'est pas obligatoire, vous pourriez utiliser un mot de passe en clair mais nous vous recommandons fortement d'utiliser un fichier `.env` comme bonne pratique. Ainsi, vous pourrez commit le fichier de configuration si nécessaire.

# Fichier de configuration

## Exemples de configuration

 - [Click here to see some examples](https://gitlab.com/codigo-pub/vscode-ext/codigo-generator-help/-/tree/master/examples?ref_type=heads)

## Configuration globale
La clé **global_config** contient la configuration globale.

### db_type
**db_type** définit le type de base de données, il peut avoir les valeurs suivantes :
- mysql
- mariadb
- postgresql

### database
**database** contient les informations sur la base de données pour se connecter à la base de données. Vous devez fournir ces informations :
- host
- user
- password
- port
- schema [optionnel] : définit le nom du schéma à utiliser. Par défaut, public. Non utilisé dans les bases de données mysql/mariaDB.
- many_to_many_sep [optionnel] définit le séparateur de chaîne entre les deux tables dans une table pivot ManyToMany. "\_has\_" par défaut.

### utilisation de l'IA

**useAI** est optionnel. Il contient des informations pour utiliser un service d'IA afin d'améliorer la complétion des fichiers générés par **codigo-generator**. Notez que bien que codigo-generator soit très rapide, l'utilisation de l'IA peut considérablement augmenter le temps de traitement. Cependant, la valeur ajoutée en vaut souvent la peine. Consultez la colonne IA dans la liste des processeurs pour vérifier si un processeur peut utiliser l'IA.

Attention, lorsque vous utilisez l'IA, vous devrez peut-être apporter des modifications au code. C'est normal.

Si vous souhaitez utiliser l'IA, vous devez fournir les informations suivantes :
- **apiKey** : Votre clé API du service d'IA. Il est fortement recommandé d'utiliser un espace réservé (ex. `${CHATGPT_APIKEY}`) et de stocker cette clé dans un fichier d'environnement !
- **enabled** : [Optionnel] Une valeur booléenne pour activer ou désactiver l'IA. La valeur par défaut est `false`.
- **aiName** : [Optionnel] Le nom du service d'IA à utiliser. Par défaut, nous utilisons le service OpenAI. Voici les valeurs possibles pour **aiName** :
  - **OpenAI** : Service API OpenAI
  - **DeepSeek** : Service API DeepSeek
  - **Mistral** : Service API Mistral
- **duration_warning** : [Optionnel] Limite de temps en secondes (par ex. `"duration_warning": 20`) avant de déclencher un avertissement pour un processus d'IA trop long. L'avertissement indique le temps écoulé et suggère d'ajuster les paramètres.


#### Propriétés supplémentaires
Ces propriétés supplémentaires sont optionnelles. Soyez prudent en les utilisant, car elles peuvent encore augmenter le temps de traitement.

- **model** : Le nom du modèle d'IA pour votre service d'IA. S'il n'est pas spécifié, une valeur par défaut est utilisée en fonction du service choisi :
  - OpenAI : **gpt-4o**
  - DeepSeek : **deepseek-chat**
  - Mistral : **ministral-8b-latest**
- **temperature** : Définit la *température* des requêtes d'IA. Non envoyé si non renseigné.
- **max_completion_tokens** : Définit le nombre maximum de tokens pour les complétions d'IA. Non envoyé si non renseigné.

Voici quelques exemples de configuration :

```JSON
"useAI": {
    "enabled": true,
    "model": "gpt-4o-mini",
    "apiKey": "${CHATGPT_APIKEY}"
}
```

```JSON
"useAI": {
    "enabled": true,
    "aiName": "DeepSeek",
    "apiKey": "${DEEPSEEK_APIKEY}"
}
```

```JSON
"useAI": {
    "enabled": true,
    "aiName": "Mistral",
    "model": "mistral-small-latest",
    "apiKey": "${MISTRAL_APIKEY}"
}
```

### template
**template** définit principalement le langage (jusqu'à présent), il peut avoir les valeurs suivantes :
- typescript
- php
- java
- python
- csharp

### packageBaseName
**packageBaseName** est optionnel mais **nécessaire pour certains modèles**, car il s'agit de votre nom de package/espace de noms de base.

Si vous utilisez le processeur de **model** avec le modèle de **php** et `"packageBaseName": "MyApp"`, vos fichiers de *model* contiendront `namespace MyApp\Models;`.

Si vous utilisez le processeur de **model** avec le modèle de **java** et `"packageBaseName": "com.myapp"`, vos fichiers de *model* contiendront `package com.myapp.models;`.

Si vous utilisez le processeur de **model** avec le modèle de **csharp** et `"packageBaseName": "AngularApp1.Server"`, vos fichiers de *model* contiendront `namespace AngularApp1.Server.Models;`.

Les processeurs Django ont besoin de `packageBaseName`, c'est le nom de l'application.

### out_folder
**out_folder** est l'endroit où les fichiers doivent être créés. Codigo-generator génère des fichiers en fonction de votre fichier de configuration, donc oui, des fichiers seront générés, et ils seront là. Cette valeur de chemin peut être :

- un **chemin relatif à cet espace de travail**, votre chemin relatif **doit commencer par un point**, par exemple => `"out_folder": "./out"`
- un **chemin absolu** => `"out_folder": "C:\\Users\\marco\\OneDrive\\tmp\\out"`

### skip_these_tables
**skip_these_tables** est optionnel. C'est un tableau de **noms de table à ignorer**.

Exemple : `"skip_these_tables": [ "classrooms" ]`

Cet exemple fera que la table **classrooms** sera ignorée lors du processus.

### only_these_tables
**only_these_tables** est optionnel. C'est un tableau de noms de tables qui seront traitées **exclusivement**.

Exemple : `"only_these_tables": [ "classrooms", "users" ]`

Cet exemple fera que les tables **classrooms** et **users** seront traitées mais pas les autres. Si **classrooms** était également dans le tableau **skip_these_tables**, elle serait ignorée (juste, n'est-ce pas ?).

## Processeurs
La clé **processors** contient un tableau de processeurs. Chaque processeur a un travail à faire... pour vous.

Chaque processeur doit avoir ces informations :
- **name** : le nom du processeur (voir ci-dessous pour une liste des noms de processeur disponibles)
- ***generate*** : est optionnel. C'est une valeur booléenne, **true** signifie que le processeur fonctionnera, **false** il sera ignoré. Par défaut, true.
- ***parameter_prefix*** : est optionnel.. C'est le texte à ajouter aux paramètres de fonction. Par défaut, c'est 'r'.
  - Exemple : `function sayHello(rName: string) { ... }`
  - avec cette configuration `"parameter_prefix": "_"`, nous aurons : `function sayHello(_Name: string) { ... }`
- ***custom_base_template*** : est optionnel. C'est le chemin absolu de votre fichier de modèle personnalisé si vous voulez quelque chose de spécial. Vous ne devriez pas utiliser cela si ce n'est pas nécessaire. Voir ci-dessous pour plus d'informations.

- ***extra_config*** : est optionnel. C'est un json avec une configuration personnalisée qui fonctionnera uniquement pour certains processeurs. Voir ci-dessous pour plus d'informations.

- ***useCamelCaseForClass*** : est optionnel. true pour utiliser le camelCase pour les noms de classes, false sinon. Par défaut, c'est true.
- ***useCamelCaseForField*** : est optionnel. true pour utiliser le camelCase pour les noms de champs, false sinon. Par défaut, c'est true. Notez que certains processeurs peuvent ignorer cette propriété en raison de la normalisation de la langue.


### Liste des processeurs
Voici une liste des processeurs disponibles pour chaque modèle. Espérons qu'elle s'agrandira avec le temps.

Certains processeurs peuvent générer des fichiers supplémentaires.

#### Modèle typescript

| nom processeur                | details                                                                   | fichiers supplémentaires              | AI ?  |
| ----------------------------- | ------------------------------------------------------------------------- |-------------------------------------- | ----- |
| model                         | Classe de modèle                                                          |                                       |       |
| angular_service               | Classe de service Angular pour les appels vers l'api back-end             | 'abstract_api.service.ts'             | oui   |
| loopback_model                | Classe de modèle du framework loopback                                    |                                       |       |
| loopback_repository           | Classe repository loopback                                                |                                       |       |
| loopback_service              | Classe service loopback                                                   |                                       | oui   |
| loopback_controller           | Classe controller loopback                                                |                                       | oui   |
| postman                       | Collection Postman (sera à la racine)                                     |                                       |       |
| angular_view_model_form       | Composant de vue de formulaire Angular pour le modèle                     | composant 'angular_generic_model_selector' | oui   |
| angular_view_model_card       | Composant de vue de carte Angular pour le modèle                          |                                       | oui   |
| angular_view_model_list       | Composant de vue de liste Angular pour le modèle                          | composant 'angular_confirm_dialog'    | oui   |
| angular_view_model_list_page  | Page Angular pour afficher le composant de vue de liste                   |                                       |       |
| ionic_angular_view_model_form       | Composant de vue de formulaire Ionic Angular pour le modèle                     | composant 'angular_generic_model_selector' | oui   |
| ionic_angular_view_model_card       | Composant de vue de carte Ionic Angular pour le modèle                          |                                       | oui   |
| ionic_angular_view_model_list       | Composant de vue de liste Ionic Angular pour le modèle                          | composant 'angular_confirm_dialog'    | oui   |
| ionic_angular_view_model_list_page  | Page Ionic Angular pour afficher le composant de vue de liste                   |                                       |       |

#### Modèle php

| nom processeur            | details                                                                   | fichiers supplémentaires              | AI ?  |
| ------------------------- | ------------------------------------------------------------------------- |-------------------------------------- | ----- |
| model                     | Classe de modèle                                                          |                                       |       |
| pdo_dao                   | Classe Dao (repository) avec PDO                                          |                                       |       |
| doctrine_entity           | Entité Doctrine pour les utilisateurs Symfony                             |                                       |       |
| doctrine_repository       | Référentiel Doctrine pour les utilisateurs Symfony                        |                                       |       |
| artisan_make_model        | Génère un seul fichier bash avec les commandes artisan make               |                                       |       |
| eloquent_model            | Fichier de modèle Eloquent pour les utilisateurs Laravel                  |                                       |       |
| symfony_service           | Service Symfony                                                           |                                       |       |
| symfony_api_controller    | Contrôleur api Symfony                                                    |                                       |       |
| postman                   | Collection Postman (sera à la racine)                                     |                                       |       |

#### Modèle java

| nom processeur            | details                                                                   | fichiers supplémentaires              | AI ?  |
| ------------------------- | ------------------------------------------------------------------------- |-------------------------------------- | ----- |
| model                     | Classe de modèle                                                          |                                       |       |
| jpa_entity                | Entité Spring JPA                                                         |                                       |       |
| jpa_repository            | Repository Spring JPA                                                     |                                       | oui   |
| spring_service            | Service Spring  qui utilise le repository                                 |                                       | oui   |
| spring_controller         | Controller Spring qui utilise le service                                  |                                       | oui   |
| postman                   | Collection Postman (sera à la racine)                                     |                                       |       |

#### Modèle Python

| Nom                    | Détails                                                              | Fichiers Supplémentaires              | AI ?  |
| ---------------------- | -------------------------------------------------------------------- | ------------------------------------- | ----- |
| django_model           | Classe de modèle Django                                              |                                       |       |
| django_import_models   | Un fichier `models.py` pour importer vos modèles, sera à la racine   |                                       |       |
| django_controller      | Classe de contrôleur Django                                          |                                       |       |
| django_controller_path | Modèles d'URL de contrôleur Django                                   |                                       |       |
| postman                | Collection Postman (sera à la racine)                                |                                       |       |

#### modèle csharp

| name                      | details                                                                       | extra files                           | AI ?  |
| ------------------------- | ----------------------------------------------------------------------------- |-------------------------------------- | ----- |
| model                     | classe de modèle                                                              |                                       |       |
| efcore_model              | classe de modèle EF Core (préférer utiliser efcore pour générer des modèles)  |                                       |       |
| efcore_repository         | repository EF Core                                                            | IAbstractRepository/AbstractRepository|       |
| net_service               | service .Net                                                                  |                                       |       |
| net_controller            | classe de contrôleur .Net                                                     |                                       |       |
| net_program               | fichier .Net Program.cs avec importation pour services/repositories           |                                       |       |
| postman                   | collection Postman (sera à la racine)                                         |                                       |       |

# custom_base_template
***custom_base_template*** est le chemin absolu de votre répertoire de modèles personnalisés si vous voulez quelque chose de spécial.

> Vous ne devriez pas l'utiliser sauf si c'est nécessaire.

**Les processeurs utilisent des fichiers de modèles pour remplir les données**. Chaque modèle reçoit des données entrantes délimitées par **{{ }}**. Si vous utilisez le vôtre, vous devez savoir ce qu'il recevra.

Vous pouvez vérifier les modèles dans le dossier `templates` sur le dépôt GitLab.

**Exemple** :

Disons que vous voulez votre propre fichier de modèle pour le modèle `java` et le processeur `model`. Tout d'abord, créez un dossier local pour stocker votre fichier de modèle. Par exemple : "c:/my_templates". Ensuite, allez dans le [dossier `templates` du dépôt de projet GitLab](https://gitlab.com/codigo-pub/vscode-ext/codigo-generator/-/tree/master/templates?ref_type=heads), puis dans le dossier `java`.

Là, vous trouverez deux fichiers liés au processeur `model` (il pourrait y en avoir un ou plusieurs) :

- tpl_model_enum.java
- tpl_model.java

Téléchargez ces deux fichiers dans votre dossier "c:/my_templates" mais **ne les renommez pas** !

Modifiez leur contenu selon vos besoins. Faites attention aux données **{{ }}**.

Ensuite, vous êtes prêt à partir. Vous pouvez exécuter votre processeur et il utilisera vos fichiers.

# extra_config
*extra_config* comme mentionné précédemment peut être utilisé uniquement avec certains processeurs.

Exemple avec une configuration supplémentaire :

```JSON
    "processors": [
        { "name": "model", "generate": false },
        { "name": "jpa_entity", "extra_config": { "lombok": false } },
        { "name": "jpa_repository", "generate": false }
    ]
```

Voici la liste des processeurs et de leurs extra_config disponibles

| modèle         | nom                      | propriété extra_config | valeur par défaut   | Détails                                            | Exemple                                               |
| -------------- | ------------------------- | --------------------- | -------------------| --------------------------------------------------- | ----------------------------------------------------- |
| java           | model                     | lombok                | true               | Ajoute les annotations lombok                       | "extra_config": { "lombok": false }                   |
| java           | jpa_entity                | lombok                | true               | Ajoute les annotations lombok                       | "extra_config": { "lombok": false }                   |
| java           | jpa_entity                | use_enum              | true               | Utiliser une classe enum lorsque enum               | "extra_config": { "use_enum": false }                 |
| java           | spring_service            | logger                | true               | Add logger                                          | "extra_config": { "logger": false }                   |
| java           | spring_controller         | logger                | true               | Add logger                                          | "extra_config": { "logger": false }                   |
| typescript     | loopback_repository       | datasource_name       | _schema name_      | Nom de la source de données pour le repository      | "extra_config": { "datasource_name": "MyApp" }        |
| php            | artisan_make_model        | option                | *chaîne vide*      | Option de la commande artisan model                 | "extra_config": { "option": "--all" }                 |
| php            | eloquent_model            | do_getters_setters    | false              | Ajoute les Getters/Setters                          | "extra_config": { "do_getters_setters": true }        |
| php            | symfony_api_controller    | base_api              | "/api"             | Base pour l'URL de l'API                            | "extra_config": { "base_api": "/myapi" }              |
| php            | symfony_api_controller    | folder_api            | "Api"              | Nom de dossier où mettre les fichiers               | "extra_config": { "folder_api": "MyApi" }             |
| java   | spring_security_jwt | user_field/pass_field/role_field  |     | Vous devez nommer les champs utilisés pour identifier l'utilisateur (email, nom d'utilisateur, ..), le mot de passe (pass, password, ...) et le rôle (nom_de_role, role, ...) | "extra_config": { "user_field": "users.email", "pass_field": "users.password", "role_field": "roles.role" } |

## postman processors

Ce sont les propriétés `extra_config` disponibles pour les processeurs `postman`.
**with_base_api** vous permet de définir si votre API a un nom de base principal. Par exemple, `/api`. Par bonne pratique, cela serait ajouté en tant que variable d'environnement `{{base_api}}` dans votre collection Postman.

``` "extra_config": { "with_base_api": false } ```

**name** vous permet de définir le nom du fichier Postman généré par le processeur. Par défaut, c'est `default-postman`. Pour Java, il peut inclure le nom du package.

``` "extra_config": { "name": "myproject-postman" } ```

## use_model_for_api_endpoint
**use_model_for_api_endpoint** est une propriété `extra_config` disponible pour les processeurs qui génèrent des fichiers avec des chemins d'API (par exemple, postman, contrôleur, services Angular, etc.).
Elle a une valeur par défaut de `false`.
Comportement :

 - true : le chemin de base du modèle utilise le nom de la table. Par exemple : /users
 - false : le chemin de base du modèle utilise le nom du modèle. Par exemple : /user

## standalone pour Angular
**standalone** est une propriété `extra_config` disponible pour les processeurs Angular permettant de créer des composants autonomes. Par défaut, c'est `true`.


# Paramètres de l'extension

Aucun paramètre d'extension pour le moment.

---


# Auteur

Marc Flausino

# Contributeurs

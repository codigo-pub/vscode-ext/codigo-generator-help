# codigo-generator

<p align="center">
    <img src="https://twini.fra1.cdn.digitaloceanspaces.com/vscode-ext/img/generator/img_codigo_generator_001.png" alt="codigo-generator" width="50%">
</p>

<p align="center">
    <img src="https://twini.fra1.cdn.digitaloceanspaces.com/vscode-ext/img/generator/mistral.png" alt="codigo-generator" width="100px">
    <img src="https://twini.fra1.cdn.digitaloceanspaces.com/vscode-ext/img/generator/openai.png" alt="codigo-generator" width="100px">
    <img src="https://twini.fra1.cdn.digitaloceanspaces.com/vscode-ext/img/generator/deepseek.png" alt="codigo-generator" width="100px">
</p>

> 🚀 Codigo Generator – Cos'è?  
- **Codigo Generator** automatizza la creazione di file di base per i tuoi progetti, combinando la generazione di codice tradizionale con IA.  
- Ti aiuta a configurare rapidamente la struttura del tuo progetto, permettendoti di concentrarti sullo sviluppo delle funzionalità invece che sulla configurazione ripetitiva.  
- Particolarmente utile quando si avvia un **nuovo progetto** o si aggiunge una **nuova funzionalità** (con nuove tabelle, ad esempio).  

> Come funziona?  
- Prende uno **schema di database** e genera più file in base alle tue necessità.  
- Se lo desideri, **Codigo Generator** può anche migliorare questi file utilizzando l'IA per ulteriori automazioni!  

> Che tipo di file genera?  
- Principalmente **modelli**, **repositories**, **services**, **controllers**, e altri file, a seconda della tua stack tecnologica.  

> Quali tecnologie sono supportate?  
- **TypeScript**, **PHP**, **Java**, **Python**, **C#**… e spero che altre ne arriveranno!  

> Framework?  
- Sì! Consulta la lista sottostante per i framework e le integrazioni supportate.  



# Demo

> Demo su youtube

[![Watch on youtube](https://img.youtube.com/vi/x60cT2zaeiA/maxresdefault.jpg)](https://www.youtube.com/watch?v=x60cT2zaeiA&list=PLEjhpI6uFRac0XlOMNjbN8WYVkDLP2zcE)


> Demo con modello php e processori symfony

![how to use](https://twini.fra1.cdn.digitaloceanspaces.com/vscode-ext/img/codigo-generator-php-symfony-demo.gif)


# Requisiti
Avrai bisogno almeno di:

- Un file `.env` nella cartella radice del tuo progetto, poiché **codigo-generator** caricherà le variabili d'ambiente tramite **dotenv** e questo file. Ci aspettiamo di avere dati segreti in questo file. Ad esempio:

```
MYSQL_DB1_ROOT_PASSWORD="OnceUponAMidnightDreary"
MYSQL_DB2_ROOT_PASSWORD="WhileIPondered"
```

- Un file di configurazione json. In quel file specificherai quale chiave vuoi usare per la password.

Esempio:

```JSON
{
    "global_config": {
        "db_type": "mysql",
        "database": {
            "host": "localhost",
            "user": "quizuser",
            "password": "${MYSQL_DB1_ROOT_PASSWORD}",
            "database": "quiz", 
            "port": 3306
        },
        "useAI": {
            "enabled": true,
            "apiKey": "${CHATGPT_APIKEY}"
        },        
        "template": "php",
        "packageBaseName": "MyApp",
        "out_folder": "./out"
    },
    "processors": [
        { "name": "doctrine_repository" },
        { "name": "doctrine_entity" }
    ]
}

```

**Nota** : Usare questo metodo non è obbligatorio, potresti usare una password normale ma ti raccomandiamo vivamente di usare un file `.env` come buona pratica. In questo modo, sarai in grado di commettere il file di configurazione se necessario.



# File di configurazione

## Esempi di configurazione

 - [Clicca qui per vedere alcuni esempi](https://gitlab.com/codigo-pub/vscode-ext/codigo-generator-help/-/tree/master/examples?ref_type=heads)

## Configurazione Globale

```markdown
La chiave **global_config** contiene la configurazione globale.

### db_type
**db_type** definisce il tipo di database, può avere questi valori:
- mysql
- mariadb
- postgresql

### database
**database** contiene le informazioni per connettersi al database. Devi fornire queste informazioni:
- host
- user
- password
- port
- schema [opzionale] definisce il nome dello schema da usare. Di default è pubblico. Non usato nei database mysql/mariaDB.
- many_to_many_sep [opzionale] definisce il separatore di stringhe tra entrambe le tabelle in una tabella pivot ManyToMany. "\_has\_" come default.

### useAI

**useAI** è opzionale. Contiene informazioni per utilizzare un servizio di IA e migliorare il completamento dei file generati da **codigo-generator**. Nota che, sebbene codigo-generator sia molto veloce, l'uso dell'IA può aumentare significativamente il tempo di elaborazione. Tuttavia, il valore aggiunto spesso ne vale la pena. Controlla la colonna IA nell'elenco dei processori per verificare se un processore può utilizzare l'IA.

Attenzione, quando utilizzi l'IA, potresti dover apportare alcune modifiche al codice. È normale.

Se vuoi utilizzare l'IA, devi fornire le seguenti informazioni:
- **apiKey**: La tua chiave API del servizio di IA. Si consiglia vivamente di utilizzare un segnaposto (es. `${CHATGPT_APIKEY}`) e di memorizzare questa chiave in un file di ambiente!
- **enabled**: [Opzionale] Un valore booleano per attivare o disattivare l'IA. Il valore predefinito è `false`.
- **aiName**: [Opzionale] Il nome del servizio di IA da utilizzare. Per impostazione predefinita, utilizziamo il servizio OpenAI. Ecco i valori possibili per **aiName**:
  - **OpenAI**: Servizio API OpenAI
  - **DeepSeek**: Servizio API DeepSeek
  - **Mistral** : Servizio API Mistral
- **duration_warning**: [Opzionale] Limite di tempo in secondi (es. `"duration_warning": 20`) prima di attivare un avviso per un processo IA troppo lungo. L'avviso indica il tempo trascorso e suggerisce di regolare le impostazioni.


#### Proprietà aggiuntive
Queste proprietà aggiuntive sono opzionali. Usale con cautela, poiché possono aumentare ulteriormente il tempo di elaborazione.

- **model**: Il nome del modello di IA per il tuo servizio di IA. Se non specificato, verrà utilizzato un valore predefinito in base al servizio scelto:
  - OpenAI: **gpt-4o**
  - DeepSeek: **deepseek-chat**
  - Mistral : **ministral-8b-latest**
- **temperature**: Definisce la *temperatura* per le richieste IA. Non verrà inviata se non fornita.
- **max_completion_tokens**: Imposta il numero massimo di token per le risposte IA. Non verrà inviata se non fornita.

Ecco alcuni esempi di configurazione:

```JSON
"useAI": {
    "enabled": true,
    "model": "gpt-4o-mini",
    "apiKey": "${CHATGPT_APIKEY}"
}
```

```JSON
"useAI": {
    "enabled": true,
    "aiName": "DeepSeek",
    "apiKey": "${DEEPSEEK_APIKEY}"
}
```

```JSON
"useAI": {
    "enabled": true,
    "aiName": "Mistral",
    "model": "mistral-small-latest",
    "apiKey": "${MISTRAL_APIKEY}"
}
```

### template
**template** definisce principalmente il linguaggio (beh, finora), può avere questi valori:
- typescript
- php
- java
- python
- csharp

### packageBaseName
**packageBaseName** è opzionale ma **necessario per alcuni modelli**, poiché questo è il tuo nome base di pacchetto/namespace.

Se usi il processore di **model** con il modello di **php** e `"packageBaseName": "MyApp"`, i tuoi file di *model* conterranno `namespace MyApp\Models;`.

Se usi il processore di **model** con il modello di **java** e `"packageBaseName": "com.myapp"`, i tuoi file di *model* conterranno `package com.myapp.models;`.

Se usi il processore di **model** con il modello di **csharp** e `"packageBaseName": "AngularApp1.Server"`, i tuoi file di *model* conterranno `namespace AngularApp1.Server.Models;`.

I processori Django necessitano di `packageBaseName`, che è il nome dell'applicazione.

### out_folder
**out_folder** è dove devono essere creati i file. Codigo-generator genera file in base al tuo file di configurazione, quindi sì, ci saranno file generati, e saranno lì. Questo valore di percorso può essere:

- un **percorso relativo a questo workspace**, il tuo percorso relativo **deve iniziare con un punto**, ad esempio => `"out_folder": "./out"`
- un **percorso assoluto** => `"out_folder": "C:\\Users\\marco\\OneDrive\\tmp\\out"`

### skip_these_tables
**skip_these_tables** è opzionale. È un array di **nomi di tabelle da saltare**.

Esempio: `"skip_these_tables": [ "classrooms" ]`

Questo esempio farà saltare la tabella **classrooms** dal processo.

### only_these_tables
**only_these_tables** è opzionale. È un array di nomi di tabelle che saranno **processate esclusivamente**.

Esempio: `"only_these_tables": [ "classrooms", "users" ]`

Questo esempio farà processare le tabelle **classrooms** e **users** ma non le altre. Nota che se **classrooms** fosse anche nell'array **skip_these_tables**, sarebbe saltata (giusto, no?).


## Processori
La chiave **processors** contiene un array di processori. Ogni processore ha un lavoro da fare... per te.

Ogni processore deve avere queste informazioni:
- **name**: il nome del processore (vedi sotto per un elenco dei nomi dei processori disponibili)
- ***generate***: è opzionale. È un valore booleano, **true** significa che il processore lavorerà, **false** che sarà saltato. Default è true.
- ***parameter_prefix***: è opzionale. Questo è il testo da aggiungere ai parametri della funzione. Di default è 'r'.
  - Esempio: `function sayHello(rName: string) { ... }`
  - con questa configurazione `"parameter_prefix": "_"` avremo: `function sayHello(_Name: string) { ... }`
- ***custom_base_template***: è opzionale. È il percorso assoluto del tuo file di modello personalizzato se desideri qualcosa di speciale. Non dovresti usarlo se non è necessario. Vedi sotto per maggiori informazioni.
- ***extra_config***: è opzionale. È un json con configurazione personalizzata che funzionerà solo per alcuni processori. Vedi sotto per maggiori informazioni.

- ***useCamelCaseForClass*** : è opzionale. true per usare il camelCase per i nomi delle classi, false altrimenti. Il valore predefinito è true.
- ***useCamelCaseForField*** : è opzionale. true per usare il camelCase per i nomi dei campi, false altrimenti. Il valore predefinito è true. Tieni presente che alcuni processori potrebbero ignorare questa proprietà a causa della normalizzazione della lingua.


### Elenco dei Processori
Ecco un elenco dei processori disponibili per ogni modello. Speriamo che cresca nel tempo.

Alcuni processori possono generare file extra.

#### modello typescript

| name                          | dettagli                                                                      | file extra                            | AI ?  |
| ----------------------------- | ----------------------------------------------------------------------------- | ------------------------------------- | ----- |
| model                         | Classe modello                                                                |                                       |       |
| angular_service               | Classe Angular Service per chiamare le api del back-end                       | 'abstract_api.service.ts'             | sì    |
| row_mapper                    | Classe Rowmapper, come una classe modello, ma solo con relazioni con i figli  |                                       |       |
| loopback_model                | Classe modello del framework loopback                                         |                                       |       |
| loopback_repository           | repository di classi del framework loopback                                   |                                       |       |
| loopback_service              | Servizio di classi del framework loopback                                     |                                       | sì    |
| loopback_controller           | controller di classi del framework loopback                                   |                                       | sì    |
| postman                       | Collezione Postman (sarà nella radice)                                        |                                       |       |
| angular_view_model_form       | Componente di visualizzazione del modulo Angular per il modello               | componente angular_generic_model_selector | sì |
| angular_view_model_card       | Componente di visualizzazione a scheda Angular per il modello                 |                                       | sì    |
| angular_view_model_list       | Componente di visualizzazione a lista Angular per il modello                  | componente 'angular_confirm_dialog'   | sì    |
| angular_view_model_list_page  | Pagina Angular per visualizzare il componente di visualizzazione a lista      |                                       |       |
| ionic_angular_view_model_form       | Componente di visualizzazione del modulo Ionic Angular per il modello               | componente angular_generic_model_selector | sì |
| ionic_angular_view_model_card       | Componente di visualizzazione a scheda Ionic Angular per il modello                 |                                       | sì    |
| ionic_angular_view_model_list       | Componente di visualizzazione a lista Ionic Angular per il modello                  | componente 'angular_confirm_dialog'   | sì    |
| ionic_angular_view_model_list_page  | Pagina Ionic Angular per visualizzare il componente di visualizzazione a lista      |                                       |       |


#### modello php

| name                      | dettagli                                                                      | file extra                            | AI ?  |
| ------------------------- | ----------------------------------------------------------------------------- |-------------------------------------- | ----- |
| model                     | Classe modello                                                                |                                       |       |
| pdo_dao                   | Classe Dao (repository) con PDO                                               |                                       |       |
| doctrine_entity           | Entità Doctrine per gli utenti di Symfony (php 8)                             |                                       |       |
| doctrine_repository       | Repository Doctrine per gli utenti di Symfony (php 8)                         |                                       |       |
| artisan_make_model        | Genera un singolo file bash con comandi artisan make                          |                                       |       |
| eloquent_model            | File modello Eloquent per gli utenti di Laravel                               |                                       |       |
| symfony_service           | Servizio Symfony                                                              |                                       |       |
| symfony_api_controller    | Controller Symfony                                                            |                                       |       |
| postman                   | Collezione Postman (sarà nella radice)                                        |                                       |       |


#### modello java

| name                      | dettagli                                                                      | file extra                            | AI ?  |
| ------------------------- | ----------------------------------------------------------------------------- |-------------------------------------- | ----- |
| model                     | Classe modello                                                                |                                       |       |
| jpa_entity                | Entità JPA Spring                                                             |                                       |       |
| jpa_repository            | Repository JPA Spring                                                         |                                       | sì    |
| spring_service            | Servizio Spring usando il repository                                          |                                       | sì    |
| spring_controller         | Controller Spring usando il servizio                                          |                                       | sì    |
| postman                   | Collezione Postman (sarà nella radice)                                        |                                       |       |

#### Modello Python

| Nome                      | Dettagli                                                                      | File Extra                            | AI ?  |
| ------------------------- | ----------------------------------------------------------------------------- | ------------------------------------- | ----- |
| django_model              | Classe di modello Django                                                      |                                       |       |
| django_import_models      | Un file `models.py` per importare i tuoi modelli, sarà nella radice           |                                       |       |
| django_controller         | Classe del controller Django                                                  |                                       |       |
| django_controller_path    | Modelli di URL del controller Django                                          |                                       |       |
| postman                   | Collezione Postman (sarà nella radice)                                        |                                       |       |

#### modello csharp

| name                      | details                                                                   | extra files                           | AI ?  |
| ------------------------- | ------------------------------------------------------------------------- |-------------------------------------- | ----- |
| model                     | classe modello                                                            |                                       |       |
| efcore_model              | classe modello EF Core (preferisci usare efcore per generare modelli)     |                                       |       |
| efcore_repository         | repository EF Core                                                        | IAbstractRepository/AbstractRepository|       |
| net_service               | servizio .Net                                                             |                                       |       |
| net_controller            | classe controller .Net                                                    |                                       |       |
| net_program               | file .Net Program.cs con importazione per servizi/repository              |                                       |       |
| postman                   | collezione Postman (sarà nella radice)                                    |                                       |       |

# custom_base_template
***custom_base_template*** è il percorso assoluto della directory del tuo modello personalizzato se desideri qualcosa di speciale.

> Non dovresti usarlo a meno che non sia necessario.

**I processori usano file di modello per popolare i dati**. Ogni modello riceve dati in arrivo delimitati da **{{ }}**. Se usi il tuo, devi sapere cosa riceverà.

Puoi controllare i modelli nella cartella `templates` nel repository GitLab.

**Esempio**:

Diciamo che desideri il tuo file di modello per il modello `java` e il processore `model`. Per prima cosa, crea una cartella locale per memorizzare il tuo file di modello. Ad esempio: "c:/my_templates". Quindi vai alla [cartella `templates` del repository del progetto GitLab](https://gitlab.com/codigo-pub/vscode-ext/codigo-generator/-/tree/master/templates?ref_type=heads), quindi nella cartella `java`.

Lì troverai due file relativi al processore `model` (potrebbero essercene uno o più):

- tpl_model_enum.java
- tpl_model.java

Scarica questi due file nella tua cartella "c:/my_templates", ma **non rinominarli**!

Modifica il loro contenuto secondo le tue esigenze. Fai attenzione ai dati **{{ }}**.

Quindi sei pronto per partire. Puoi eseguire il tuo processore e utilizzerà i tuoi file.

# extra_config
*extra_config* come detto prima può essere utilizzato solo con alcuni processori.

Esempio con una configurazione extra:

```JSON
    "processors": [
        { "name": "model", "generate": false },
        { "name": "jpa_entity", "extra_config": { "lombok": false } },
        { "name": "jpa_repository", "generate": false }
    ]
``


Questo è l'elenco dei processori e delle loro extra_config disponibili.

| template       | name                      | extra_config property | valore di default  | dettagli                                         | esempio                                              |
| -------------- | ------------------------- | --------------------- | -------------------| ------------------------------------------------ | ---------------------------------------------------- |
| java           | model                     | lombok                | true               | Aggiungi annotazioni lombok                      | "extra_config": { "lombok": false }                  |
| java           | jpa_entity                | lombok                | true               | Aggiungi annotazioni lombok                      | "extra_config": { "lombok": false }                  |
| java           | jpa_entity                | use_enum              | true               | Usa la classe enum quando enum                   | "extra_config": { "use_enum": false }                |
| java           | spring_service            | logger                | true               | Aggiungi logger                                  | "extra_config": { "logger": false }                  |
| java           | spring_controller         | logger                | true               | Aggiungi logger                                  | "extra_config": { "logger": false }                  |
| typescript     | loopback_repository       | datasource_name       | _schema name_      | Nome dell'origine dati per il repository         | "extra_config": { "datasource_name": "MyApp" }        |
| php            | artisan_make_model        | option                | *empty string*     | Opzione del comando modello artisan              | "extra_config": { "option": "--all" }                |
| php            | eloquent_model            | do_getters_setters    | false              | Aggiungi Getters/Setters                         | "extra_config": { "do_getters_setters": true }       |
| php            | symfony_api_controller    | base_api              | "/api"             | Base per l'URL api                               | "extra_config": { "base_api": "/myapi" }             |
| php            | symfony_api_controller    | folder_api            | "Api"              | Nome della cartella dove mettere i file          | "extra_config": { "folder_api": "MyApi" }            |
| java   | spring_security_jwt | user_field/pass_field/role_field  |     | Devi nominare i campi utilizzati per identificare l'utente (email, nome utente, ..), la password (pass, password, ...) e il ruolo (nome_ruolo, role, ...) | "extra_config": { "user_field": "users.email", "pass_field": "users.password", "role_field": "roles.role" } |

## postman processors

Queste sono le proprietà `extra_config` disponibili per i processori `postman`.
**with_base_api** ti consente di definire se la tua API ha un nome base principale. Ad esempio, `/api`. Come buona pratica, questo verrebbe aggiunto come variabile d'ambiente `{{base_api}}` nella tua raccolta Postman.

``` "extra_config": { "with_base_api": false } ```

**name** ti consente di definire il nome del file Postman generato dal processore. Il valore predefinito è `default-postman`. Per Java, può includere il nome del pacchetto.

``` "extra_config": { "name": "myproject-postman" } ```

## use_model_for_api_endpoint
**use_model_for_api_endpoint** è una proprietà `extra_config` disponibile per i processori che generano file con percorsi API (ad esempio, postman, controller, servizi Angular, ecc.).
Ha un valore predefinito di `false`.
Comportamento:

 - true : il percorso base del modello usa il nome della tabella. Ad esempio: /users
 - false : il percorso base del modello usa il nome del modello. Ad esempio: /user

## standalone per Angular
**standalone** è una proprietà `extra_config` disponibile per i processori Angular per richiedere la creazione di componenti autonomi. Il valore predefinito è `true`.


# Impostazioni dell'estensione

Nessuna impostazione dell'estensione per ora.


---


# Autore

Marc Flausino

# Collaboratori


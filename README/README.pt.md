# codigo-generator

<p align="center">
    <img src="https://twini.fra1.cdn.digitaloceanspaces.com/vscode-ext/img/generator/img_codigo_generator_001.png" alt="codigo-generator" width="50%">
</p>

<p align="center">
    <img src="https://twini.fra1.cdn.digitaloceanspaces.com/vscode-ext/img/generator/mistral.png" alt="codigo-generator" width="100px">
    <img src="https://twini.fra1.cdn.digitaloceanspaces.com/vscode-ext/img/generator/openai.png" alt="codigo-generator" width="100px">
    <img src="https://twini.fra1.cdn.digitaloceanspaces.com/vscode-ext/img/generator/deepseek.png" alt="codigo-generator" width="100px">
</p>

> 🚀 Codigo Generator – O que é?  
- **Codigo Generator** automatiza a criação de arquivos base para seus projetos, combinando geração de código tradicional com IA.  
- Ele ajuda a configurar rapidamente a estrutura do seu projeto, permitindo que você se concentre no desenvolvimento das funcionalidades em vez de na configuração repetitiva.  
- Particularmente útil ao iniciar um **novo projeto** ou adicionar uma **nova funcionalidade** (com novas tabelas, por exemplo).  

> Como funciona?  
- Ele pega um **esquema de banco de dados** e gera vários arquivos de acordo com suas necessidades.  
- Se você quiser, **Codigo Generator** também pode aprimorar esses arquivos usando IA para mais automação!  

> Quais tipos de arquivos ele gera?  
- Principalmente **modelos**, **repositories**, **services**, **controllers**, e outros arquivos, dependendo da sua stack tecnológica.  

> Quais tecnologias são suportadas?  
- **TypeScript**, **PHP**, **Java**, **Python**, **C#**… e espero que mais a caminho!  

> Frameworks?  
- Sim! Consulte a lista abaixo para frameworks e integrações suportadas.  


# Demo

> Demo no youtube

[![Watch on youtube](https://img.youtube.com/vi/x60cT2zaeiA/maxresdefault.jpg)](https://www.youtube.com/watch?v=x60cT2zaeiA&list=PLEjhpI6uFRac0XlOMNjbN8WYVkDLP2zcE)


> Demonstração com modelo php e processadores symfony

![como usar](https://twini.fra1.cdn.digitaloceanspaces.com/vscode-ext/img/codigo-generator-php-symfony-demo.gif)


# Requisitos
Você precisará no mínimo de:

- Um arquivo `.env` na pasta raiz do seu projeto, pois o **codigo-generator** carregará variáveis de ambiente através do **dotenv** e deste arquivo. Esperamos ter dados secretos neste arquivo. Por exemplo:

```
MYSQL_DB1_ROOT_PASSWORD="OnceUponAMidnightDreary"
MYSQL_DB2_ROOT_PASSWORD="WhileIPondered"
```

- Um arquivo de configuração json. Você dirá neste arquivo qual chave você quer usar para a senha.

Exemplo:

```JSON
{
    "global_config": {
        "db_type": "mysql",
        "database": {
            "host": "localhost",
            "user": "quizuser",
            "password": "${MYSQL_DB1_ROOT_PASSWORD}",
            "database": "quiz",
            "port": 3306
        },
        "useAI": {
            "enabled": true,
            "apiKey": "${CHATGPT_APIKEY}"
        },        
        "template": "php",
        "packageBaseName": "MyApp",
        "out_folder": "./out"
    },
    "processors": [
        { "name": "doctrine_repository" },
        { "name": "doctrine_entity" }
    ]
}
```

**Nota**: Usar este método não é obrigatório, você poderia usar uma senha simples, mas recomendamos fortemente o uso de um arquivo `.env` como boa prática. Assim, você poderá comprometer o arquivo de configuração se necessário.

# Arquivo de Configuração

## Exemplos de configuração

 - [Click here to see some examples](https://gitlab.com/codigo-pub/vscode-ext/codigo-generator-help/-/tree/master/examples?ref_type=heads)

## Configuração Global
A chave **global_config** contém a configuração global.

### db_type
**db_type** define o tipo de banco de dados, ele pode ter os seguintes valores:
- mysql
- mariadb
- postgresql

### database
**database** contém informações do banco de dados para conectar ao banco de dados. Você deve fornecer essas informações:
- host
- user
- password
- port
- schema [opcional]: define o nome do esquema a ser usado. Padrão é public. Não utilizado em bancos de dados mysql/mariaDB.
- many_to_many_sep [opcional] define o separador entre ambas as tabelas em uma tabela pivô ManyToMany. "\_has\_" por padrão.

### useAI

**useAI** é opcional. Ele contém informações para usar um serviço de IA para aprimorar a conclusão dos arquivos gerados pelo **codigo-generator**. Observe que, embora o codigo-generator seja muito rápido, o uso de IA pode aumentar significativamente o tempo de processamento. No entanto, o valor agregado muitas vezes compensa. Verifique a coluna de IA na lista de processadores para ver se um processador pode usar IA.

Atenção, ao usar IA, pode ser necessário fazer algumas alterações no código. Isso é normal.

Se você deseja usar IA, deve fornecer as seguintes informações:
- **apiKey**: Sua chave de API do serviço de IA. É altamente recomendável usar um espaço reservado (ex. `${CHATGPT_APIKEY}`) e armazenar essa chave em um arquivo de ambiente!
- **enabled**: [Opcional] Um valor booleano para ativar ou desativar a IA. O valor padrão é `false`.
- **aiName**: [Opcional] O nome do serviço de IA a ser utilizado. Por padrão, usamos o serviço OpenAI. Aqui estão os valores possíveis para **aiName**:
  - **OpenAI**: Serviço API OpenAI
  - **DeepSeek**: Serviço API DeepSeek
  - **Mistral** : Serviço API Mistral
- **duration_warning**: [Opcional] Limite de tempo em segundos (por ex. `"duration_warning": 20`) antes de acionar um aviso para um processo de IA demorando muito. O aviso indica o tempo decorrido e sugere ajustar as configurações.


#### Propriedades adicionais
Essas propriedades adicionais são opcionais. Tenha cuidado ao usá-las, pois podem aumentar ainda mais o tempo de processamento.

- **model**: O nome do modelo de IA para seu serviço de IA. Se não for especificado, um valor padrão será usado de acordo com o serviço escolhido:
  - OpenAI: **gpt-4o**
  - DeepSeek: **deepseek-chat**
  - Mistral : **ministral-8b-latest**
- **temperature**: Define a *temperatura* para as requisições de IA. Não será enviada se não for fornecida.
- **max_completion_tokens**: Define o número máximo de tokens para as conclusões de IA. Não será enviado se não for fornecido.

Aqui estão alguns exemplos de configuração:

```JSON
"useAI": {
    "enabled": true,
    "model": "gpt-4o-mini",
    "apiKey": "${CHATGPT_APIKEY}"
}
```

```JSON
"useAI": {
    "enabled": true,
    "aiName": "DeepSeek",
    "apiKey": "${DEEPSEEK_APIKEY}"
}
```

```JSON
"useAI": {
    "enabled": true,
    "aiName": "Mistral",
    "model": "mistral-small-latest",
    "apiKey": "${MISTRAL_APIKEY}"
}
```

### template
**template** define principalmente a linguagem (até agora), ele pode ter os seguintes valores:
- typescript
- php
- java
- python
- csharp

### packageBaseName
**packageBaseName** é opcional, mas **necessário para alguns modelos**, pois é o nome do pacote/nome do espaço de nomes base.

Se você usar o processador de **model** com o modelo de **php** e `"packageBaseName": "MyApp"`, seus arquivos de *model* conterão `namespace MyApp\Models;`.

Se você usar o processador de **model** com o modelo de **java** e `"packageBaseName": "com.myapp"`, seus arquivos de *model* conterão `package com.myapp.models;`.

Se você usar o processador de **model** com o modelo de **csharp** e `"packageBaseName": "AngularApp1.Server"`, seus arquivos de *model* conterão `namespace AngularApp1.Server.Models;`.

Os processadores Django precisam de `packageBaseName`, que é o nome do aplicativo.

### out_folder
**out_folder** é onde os arquivos devem ser criados. Codigo-generator gera arquivos com base no seu arquivo de configuração, então sim, arquivos serão gerados e eles estarão lá. Este valor de caminho pode ser:

- um **caminho relativo a este espaço de trabalho**, seu caminho relativo **deve começar com um ponto**, por exemplo => `"out_folder": "./out"`
- um **caminho absoluto** => `"out_folder": "C:\\Users\\marco\\OneDrive\\tmp\\out"`

### skip_these_tables
**skip_these_tables** é opcional. É um array de **nomes de tabela para ignorar**.

Exemplo: `"skip_these_tables": [ "classrooms" ]`

Este exemplo fará com que a tabela **classrooms** seja ignorada no processo.

### only_these_tables
**only_these_tables** é opcional. É uma tabela de nomes de tabelas que serão tratadas **exclusivamente**.

Exemplo: `"only_these_tables": [ "classrooms", "users" ]`

Este exemplo fará com que as tabelas **classrooms** e **users** sejam tratadas, mas não as outras. Se **classrooms** também estivesse na tabela **skip_these_tables**, ela seria ignorada (justo, não é?).

## Processadores
A chave **processors** contém uma matriz de processadores. Cada processador tem um trabalho a fazer... para você.

Cada processador deve ter estas informações:
- **name**: o nome do processador (veja abaixo para uma lista dos nomes de processador disponíveis)
- ***generate***: é opcional. É um valor booleano, **true** significa que o processador funcionará, **false** ele será ignorado. Por padrão, true.
- ***parameter_prefix***: é opcional.. Este é o texto a ser anexado aos parâmetros da função. Por padrão, é 'r'.
  - Exemplo: `function sayHello(rName: string) { ... }`
  - com esta configuração `"parameter_prefix": "_"`, teremos: `function sayHello(_Name: string) { ... }`
- ***custom_base_template***: é opcional. É o caminho absoluto do seu arquivo de modelo personalizado se você quiser algo especial. Você não deve usar isso se não for necessário. Veja abaixo para mais informações.

- ***extra_config***: é opcional. É um json com configuração personalizada que funcionará apenas para alguns processadores. Veja abaixo para mais informações.

- ***useCamelCaseForClass*** : é opcional. true para usar camelCase para nomes de classes, false caso contrário. O valor padrão é true.
- ***useCamelCaseForField*** : é opcional. true para usar camelCase para nomes de campos, false caso contrário. O valor padrão é true. Note que alguns processadores podem ignorar esta propriedade devido à normalização de idiomas.


### Lista de Processadores
Aqui está uma lista de processadores disponíveis para cada modelo. Esperamos que ela cresça com o tempo.

Alguns processadores podem gerar arquivos extras.

#### Modelo typescript

| name                          | detalhes                                                                  | arquivos extras                       | AI ?  |
| ----------------------------- | ------------------------------------------------------------------------- |-------------------------------------- | ----- |
| model                         | Classe de modelo                                                          |                                       |       |
| angular_service               | Classe de serviço Angular para chamadas à API back-end                    | 'abstract_api.service.ts'             | sim   |
| loopback_model                | Modelo de classe do framework loopback                                    |                                       |       |
| loopback_repository           | repositório de classe do framework loopback                               |                                       |       |
| loopback_service              | serviço de classe do framework loopback                                   |                                       | sim   |
| loopback_controller           | controlador de classe do framework loopback                               |                                       | sim   |
| postman                       | Coleção do Postman (estará na raiz)                                       |                                       |       |
| angular_view_model_form       | Componente de formulário de visualização Angular para o modelo            | componente angular_generic_model_selector | sim |
| angular_view_model_card       | Componente de visualização em cartão Angular para o modelo                |                                       | sim   |
| angular_view_model_list       | Componente de visualização em lista Angular para o modelo                 | componente 'angular_confirm_dialog'   | sim   |
| angular_view_model_list_page  | Página Angular para mostrar o componente de visualização em lista         |                                       |       |
| ionic_angular_view_model_form       | Componente de formulário de visualização Ionic Angular para o modelo            | componente angular_generic_model_selector | sim |
| ionic_angular_view_model_card       | Componente de visualização em cartão Ionic Angular para o modelo                |                                       | sim   |
| ionic_angular_view_model_list       | Componente de visualização em lista Ionic Angular para o modelo                 | componente 'angular_confirm_dialog'   | sim   |
| ionic_angular_view_model_list_page  | Página Ionic Angular para mostrar o componente de visualização em lista         |                                       |       |


#### Modelo php

| name                      | detalhes                                                                  | arquivos extras                       | AI ?  |
| ------------------------- | ------------------------------------------------------------------------- |-------------------------------------- | ----- |
| model                     | Classe de modelo                                                          |                                       |       |
| pdo_dao                   | Classe Dao (repositório) com PDO                                          |                                       |       |
| doctrine_entity           | Entidade Doctrine para usuários Symfony                                   |                                       |       |
| doctrine_repository       | Repositório Doctrine para usuários Symfony                                |                                       |       |
| artisan_make_model        | Gera um único arquivo bash com os comandos de criação do artisan          |                                       |       |
| eloquent_model            | Arquivo de modelo Eloquent para usuários Laravel                          |                                       |       |
| symfony_service           | Serviço Symfony                                                           |                                       |       |
| symfony_api_controller    | Controlador api Symfony                                                   |                                       |       |
| postman                   | Coleção do Postman (estará na raiz)                                       |                                       |       |

#### Modelo java

| name                      | detalhes                                                                  | arquivos extras                       | AI ?  |
| ------------------------- | ------------------------------------------------------------------------- |-------------------------------------- | ----- |
| model                     | Classe de modelo                                                          |                                       |       |
| jpa_entity                | Entidade Spring JPA                                                       |                                       |       |
| jpa_repository            | Repositório Spring JPA                                                    |                                       | sim   |
| spring_service            | Spring service usando repositório                                         |                                       | sim   |
| spring_controller         | Spring controller usando serviço                                          |                                       | sim   |
| postman                   | Coleção do Postman (estará na raiz)                                       |                                       |       |

#### Modelo Python

| Nome                   | Detalhes                                                             | Arquivos Extras                       | AI ?  |
| ---------------------- | -------------------------------------------------------------------- | ------------------------------------- | ----- |
| django_model           | Classe de modelo Django                                              |                                       |       |
| django_import_models   | Um arquivo `models.py` para importar seus modelos, estará na raiz    |                                       |       |
| django_controller      | Classe de controlador Django                                         |                                       |       |
| django_controller_path | Padrões de URL de controlador Django                                 |                                       |       |
| postman                | Coleção do Postman (estará na raiz)                                  |                                       |       |

#### modelo csharp

| Nome                      | Detalhes                                                                  | Arquivos Extras                       | AI ?  |
| ------------------------- | ------------------------------------------------------------------------- |-------------------------------------- | ----- |
| model                     | classe de modelo                                                          |                                       |       |
| efcore_model              | classe de modelo EF Core (preferir usar efcore para gerar modelos)        |                                       |       |
| efcore_repository         | repositório EF Core                                                       | IAbstractRepository/AbstractRepository|       |
| net_service               | serviço .Net                                                              |                                       |       |
| net_controller            | classe de controlador .Net                                                |                                       |       |
| net_program               | arquivo .Net Program.cs com importação para serviços/repositórios         |                                       |       |
| postman                   | coleção do Postman (estará na raiz)                                       |                                       |       |

# custom_base_template
***custom_base_template*** é o caminho absoluto do seu diretório de modelos personalizados se você quiser algo especial.

> Você não deve usar isso a menos que seja necessário.

**Os processadores usam arquivos de modelo para preencher dados**. Cada modelo recebe dados de entrada delimitados por **{{ }}**. Se você usar o seu próprio, deve saber o que ele receberá.

Você pode verificar os modelos na pasta `templates` no repositório GitLab.

**Exemplo**:

Digamos que você queira seu próprio arquivo de modelo para o modelo `java` e o processador `model`. Primeiro, crie uma pasta local para armazenar seu arquivo de modelo. Por exemplo: "c:/my_templates". Em seguida, vá para a [pasta `templates` do repositório do projeto GitLab](https://gitlab.com/codigo-pub/vscode-ext/codigo-generator/-/tree/master/templates?ref_type=heads), depois na pasta `java`.

Lá você encontrará dois arquivos relacionados ao processador `model` (pode haver um ou mais):

- tpl_model_enum.java
- tpl_model.java

Baixe esses dois arquivos para a sua pasta "c:/my_templates", mas **não os renomeie**!

Altere seus conteúdos conforme suas necessidades. Tenha cuidado com os dados **{{ }}**.

Então, você está pronto para ir. Você pode executar seu processador e ele usará seus arquivos.

# extra_config
*extra_config* como mencionado anteriormente pode ser usado apenas com alguns processadores.

Exemplo com uma configuração extra:

```JSON
    "processors": [
        { "name": "model", "generate": false },
        { "name": "jpa_entity", "extra_config": { "lombok": false } },
        { "name": "jpa_repository", "generate": false }
    ]
```

Aqui está a lista de processadores e suas extra_config disponíveis

| modelo         | nome                      | propriedade extra_config | valor padrão          | Detalhes                                          | Exemplo                                               |
| -------------- | ------------------------- | ------------------------ | --------------------- | ------------------------------------------------- | ----------------------------------------------------- |
| java           | model                     | lombok                   | true                  | Agregar anotaciones de lombok                     | "extra_config": { "lombok": false }                   |
| java           | jpa_entity                | lombok                   | true                  | Agregar anotaciones de lombok                     | "extra_config": { "lombok": false }                   |
| java           | jpa_entity                | use_enum                 | true                  | Usar classe enum quando enum                      | "extra_config": { "use_enum": false }                 |
| java           | spring_service            | logger                   | true                  | Agregar logger                                    | "extra_config": { "logger": false }                   |
| java           | spring_controller         | logger                   | true                  | Agregar logger                                    | "extra_config": { "logger": false }                   |
| typescript     | loopback_repository       | datasource_name          | _schema name_         | Nome da fonte de dados para o repositório         | "extra_config": { "datasource_name": "MyApp" }        |
| php            | artisan_make_model        | option                   | *empty string*        | Opción del comando de modelo Artisan              | "extra_config": { "option": "--all" }                 |
| php            | eloquent_model            | do_getters_setters       | false                 | Agregar Getters/Setters                           | "extra_config": { "do_getters_setters": true }        |
| php            | symfony_api_controller    | base_api                 | "/api"                | Base para URL da API                              | "extra_config": { "base_api": "/myapi" }              |
| php            | symfony_api_controller    | folder_api               | "Api"                 | Nome da pasta onde colocar os arquivos            | "extra_config": { "folder_api": "MyApi" }             |
| java   | spring_security_jwt | user_field/pass_field/role_field  |     | Você deve nomear os campos usados para identificar o usuário (email, nome de usuário, ..), a senha (pass, password, ...) e o papel (nome_do_papel, role, ...) | "extra_config": { "user_field": "users.email", "pass_field": "users.password", "role_field": "roles.role" } |

## postman processors

Estas são as propriedades `extra_config` disponíveis para os processadores `postman`.
**with_base_api** permite definir se sua API tem um nome base principal. Por exemplo, `/api`. Como boa prática, isso seria adicionado como uma variável de ambiente `{{base_api}}` na sua coleção Postman.

``` "extra_config": { "with_base_api": false } ```

**name** permite definir o nome do arquivo Postman gerado pelo processador. O padrão é `default-postman`. Para Java, pode incluir o nome do pacote.

``` "extra_config": { "name": "myproject-postman" } ```

## use_model_for_api_endpoint
**use_model_for_api_endpoint** é uma propriedade `extra_config` disponível para os processadores que geram arquivos com caminho de API (por exemplo, postman, controlador, serviços Angular, etc.).
O valor padrão é `false`.
Comportamento:

 - true : o caminho base do modelo usa o nome da tabela. Por exemplo: /users
 - false : o caminho base do modelo usa o nome do modelo. Por exemplo: /user

## standalone para Angular
**standalone** é uma propriedade `extra_config` disponível para os processadores Angular que solicitam a criação de componentes independentes. O valor padrão é `true`.

# Configurações da Extensão

Nenhuma configuração de extensão por enquanto.


---


# Autor

Marc Flausino

# Contribuidores

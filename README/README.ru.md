# codigo-generator

<p align="center">
    <img src="https://twini.fra1.cdn.digitaloceanspaces.com/vscode-ext/img/generator/img_codigo_generator_001.png" alt="codigo-generator" width="50%">
</p>

<p align="center">
    <img src="https://twini.fra1.cdn.digitaloceanspaces.com/vscode-ext/img/generator/mistral.png" alt="codigo-generator" width="100px">
    <img src="https://twini.fra1.cdn.digitaloceanspaces.com/vscode-ext/img/generator/openai.png" alt="codigo-generator" width="100px">
    <img src="https://twini.fra1.cdn.digitaloceanspaces.com/vscode-ext/img/generator/deepseek.png" alt="codigo-generator" width="100px">
</p>

> 🚀 Codigo Generator – Что это?  
- **Codigo Generator** автоматизирует создание базовых файлов для ваших проектов, сочетая традиционную генерацию кода с ИИ.  
- Он помогает быстро настроить структуру вашего проекта, чтобы вы могли сосредоточиться на разработке функционала, а не на повторяющихся настройках.  
- Особенно полезен при запуске **нового проекта** или добавлении **новой функции** (например, с новыми таблицами).  

> Как это работает?  
- Он берет **схему базы данных** и генерирует несколько файлов в зависимости от ваших потребностей.  
- Если хотите, **Codigo Generator** также может улучшить эти файлы с помощью ИИ для еще большего уровня автоматизации!  

> Какие файлы он генерирует?  
- В основном **модели**, **репозитории**, **сервисы**, **контроллеры** и другие файлы, в зависимости от вашего технологического стека.  

> Какие технологии поддерживаются?  
- **TypeScript**, **PHP**, **Java**, **Python**, **C#**… и надеюсь, что в будущем появятся новые!  

> Какие фреймворки?  
- Да! Посмотрите ниже для списка поддерживаемых фреймворков и интеграций.  


# Демо

> Демонстрация на YouTube

[![Watch on youtube](https://img.youtube.com/vi/x60cT2zaeiA/maxresdefault.jpg)](https://www.youtube.com/watch?v=x60cT2zaeiA&list=PLEjhpI6uFRac0XlOMNjbN8WYVkDLP2zcE)


> Демо с шаблоном php и процессорами symfony

![как использовать](https://twini.fra1.cdn.digitaloceanspaces.com/vscode-ext/img/codigo-generator-php-symfony-demo.gif)


# Требования
Вам понадобится как минимум:

- Файл `.env` в корневой папке вашего проекта, поскольку **codigo-generator** загрузит переменные среды через **dotenv** и этот файл. Мы ожидаем, что в этом файле будут секретные данные. Например:

```
MYSQL_DB1_ROOT_PASSWORD="OnceUponAMidnightDreary"
MYSQL_DB2_ROOT_PASSWORD="WhileIPondered"
```

JSON-файл конфигурации. В этом файле вы укажете, какой ключ вы хотите использовать для пароля.
Пример:

```JSON
{
    "global_config": {
        "db_type": "mysql",
        "database": {
            "host": "localhost",
            "user": "quizuser",
            "password": "${MYSQL_DB1_ROOT_PASSWORD}",
            "database": "quiz", 
            "port": 3306
        },
        "useAI": {
            "enabled": true,
            "apiKey": "${CHATGPT_APIKEY}"
        },        
        "template": "php",
        "packageBaseName": "MyApp",
        "out_folder": "./out"
    },
    "processors": [
        { "name": "doctrine_repository" },
        { "name": "doctrine_entity" }
    ]
}
```

**Note:** Использование этого метода не является обязательным, вы можете использовать обычный пароль, но мы настоятельно рекомендуем вам использовать файл `.env` как хорошую практику. Таким образом, вы сможете зафиксировать файл конфигурации при необходимости.

# Файл конфигурации

## Примеры конфигурации

 - [Нажмите здесь, чтобы увидеть примеры](https://gitlab.com/codigo-pub/vscode-ext/codigo-generator-help/-/tree/master/examples?ref_type=heads)

## Глобальная конфигурация
Ключ **global_config** содержит глобальную конфигурацию.

### db_type
**db_type** определяет тип базы данных, он может иметь следующие значения: 
- mysql
- mariadb
- postgresql

### database
**database** содержит информацию о базе данных для подключения к ней. Вы должны предоставить следующую информацию: 
- host
- user
- password
- port
- schema [необязательно]: определение имени схемы для использования. По умолчанию public. Не используется в базах данных mysql/mariaDB.
- many_to_many_sep [опционально] определяет строковый разделитель между обеими таблицами в сводной таблице ManyToMany. "\_has\_" по умолчанию.

### useAI

**useAI** является необязательным. Он содержит информацию об использовании сервиса ИИ для улучшения завершения файлов, создаваемых с помощью **codigo-generator**. Обратите внимание, что хотя **codigo-generator** работает очень быстро, использование ИИ может значительно увеличить время обработки. Однако добавленная ценность часто оправдывает это. Проверьте столбец ИИ в списке процессоров, чтобы узнать, может ли процессор использовать ИИ.

Будьте осторожны: при использовании ИИ может потребоваться внести изменения в код. Это нормально.

Если вы хотите использовать ИИ, необходимо предоставить следующую информацию:
- **apiKey**: Ваш API-ключ сервиса ИИ. Настоятельно рекомендуется использовать заполнитель (например, `${CHATGPT_APIKEY}`) и хранить этот ключ в файле окружения!
- **enabled**: [Опционально] Логическое значение для включения или отключения ИИ. По умолчанию установлено `false`.
- **aiName**: [Опционально] Название сервиса ИИ, который будет использоваться. По умолчанию используется сервис OpenAI. Возможные значения **aiName**:
  - **OpenAI**: API-сервис OpenAI
  - **DeepSeek**: API-сервис DeepSeek
  - **Mistral** : API-сервис Mistral
- **duration_warning**: [Опционально] Ограничение времени в секундах (напр. `"duration_warning": 20`) перед выдачей предупреждения о слишком долгом процессе ИИ. Предупреждение указывает прошедшее время и предлагает настроить параметры.


#### Дополнительные свойства
Эти свойства являются необязательными. Будьте осторожны при их использовании, так как они могут еще больше увеличить время обработки.

- **model**: Название модели ИИ для выбранного сервиса. Если параметр не указан, будет использоваться значение по умолчанию, зависящее от выбранного сервиса:
  - OpenAI: **gpt-4o**
  - DeepSeek: **deepseek-chat**
  - Mistral : **ministral-8b-latest**
- **temperature**: Определяет *температуру* для запросов ИИ. Не будет отправлено, если не указано.
- **max_completion_tokens**: Устанавливает максимальное количество токенов для завершений ИИ. Не будет отправлено, если не указано.

Вот некоторые примеры конфигурации:

```JSON
"useAI": {
    "enabled": true,
    "model": "gpt-4o-mini",
    "apiKey": "${CHATGPT_APIKEY}"
}
```

```JSON
"useAI": {
    "enabled": true,
    "aiName": "DeepSeek",
    "apiKey": "${DEEPSEEK_APIKEY}"
}
```

```JSON
"useAI": {
    "enabled": true,
    "aiName": "Mistral",
    "model": "mistral-small-latest",
    "apiKey": "${MISTRAL_APIKEY}"
}
```

### template
**template** определяет язык в основном (ну, пока), он может иметь следующие значения: 
- typescript
- php
- java
- python
- csharp

### packageBaseName
**packageBaseName** необязателен, но **необходим для некоторых шаблонов**, так как это ваше базовое имя пакета/пространства имен.

Если вы используете процессор **model** с шаблоном **php** и `"packageBaseName": "MyApp"`, ваши файлы *model* будут содержать `namespace MyApp\Models;`.

Если вы используете процессор **model** с шаблоном **java** и `"packageBaseName": "com.myapp"`, ваши файлы *model* будут содержать `package com.myapp.models;`.

Если вы используете процессор **model** с шаблоном **csharp** и `"packageBaseName": "AngularApp1.Server"`, ваши файлы *model* будут содержать `namespace AngularApp1.Server.Models;`.

Процессоры Django требуют `packageBaseName`, это имя приложения.

### out_folder
**out_folder** - это место, где должны быть созданы файлы. Кодогенератор создает файлы на основе вашего файла конфигурации, так что да, файлы будут созданы, и они будут там. Значение этого пути может быть: 

- **относительный путь к этому рабочему пространству**, ваш относительный путь **должен начинаться с точки**, например => `"out_folder": "./out"`
- **абсолютный путь** => `"out_folder": "C:\\Users\\marco\\OneDrive\\tmp\\out"`

### skip_these_tables
**skip_these_tables** необязателен. Это массив **имен таблиц для пропуска**.

Пример: `"skip_these_tables": [ "classrooms" ]`

Этот пример приведет к пропуску таблицы **classrooms** в процессе.

### only_these_tables
**only_these_tables** является необязательным. Это таблица имен таблиц, которые будут обрабатываться **исключительно**.

Пример: `"only_these_tables": [ "classrooms", "users" ]`

Этот пример приведет к тому, что таблицы **classrooms** и **users** будут обрабатываться, но не другие. Если **classrooms** также была в таблице **skip_these_tables**, она будет проигнорирована (справедливо, не так ли?).

## Процессоры
Ключ **processors** содержит массив процессоров. У каждого процессора есть своя работа... для вас.

У каждого процессора должна быть следующая информация:
- **name**: имя процессора (см. ниже список доступных имен процессоров)
- ***generate***: необязателен. Это булево значение, **true** означает, что процессор будет работать, **false** - он будет пропущен. По умолчанию true.
- ***parameter_prefix***: необязателен. Это текст, добавляемый к параметрам функции. По умолчанию он равен 'r'.
  - Пример: `function
- ***custom_base_template*** : это необязательно. Это абсолютный путь к вашему пользовательскому файлу шаблона, если вам что-то особенное нужно. Используйте это только если это необходимо. Смотрите ниже для более подробной информации.

- ***extra_config*** : это необязательно. Это JSON с пользовательской конфигурацией, которая будет работать только для некоторых процессоров. Смотрите ниже для более подробной информации.

- ***useCamelCaseForClass*** : является необязательным. true для использования camelCase для имен классов, false в противном случае. Значение по умолчанию — true.
- ***useCamelCaseForField*** : является необязательным. true для использования camelCase для имен полей, false в противном случае. Значение по умолчанию — true. Обратите внимание, что некоторые процессоры могут игнорировать это свойство из-за нормализации языка.


### Список процессоров
Вот список доступных процессоров для каждого шаблона. Надеемся, что список будет расширяться со временем.

Некоторые процессоры могут генерировать дополнительные файлы.

#### Шаблон typescript

| name                          | details                                                                               | extra files                           | AI ?  |
| ----------------------------- | ------------------------------------------------------------------------------------- |-------------------------------------- | ----- |
| model                         | Модель класса                                                                         |                                       |       |
| angular_service               | Класс Angular Service для вызова API с сервера                                        | 'abstract_api.service.ts'             | да    |
| row_mapper                    | Класс Rowmapper, подобный классу модели, но только с отношениями к дочерним элементам |                                       |       |
| loopback_model                | Класс модели фреймворка loopback                                                      |                                       |       |
| loopback_repository           | репозиторий класса фреймворка loopback                                                |                                       |       |
| loopback_service              | Service класса фреймворка LoopBack                                                    |                                       | да    |
| loopback_controller           | контроллер класса фреймворка loopback                                                 |                                       | да    |
| postman                       | Коллекция Postman (будет в корневом каталоге)                                         |                                       |       |
| angular_view_model_form       | Компонент формы представления Angular для модели                                      | компонент angular_generic_model_selector | да |
| angular_view_model_card       | Компонент карточного представления Angular для модели                                 |                                       | да |
| angular_view_model_list       | Компонент списка представления Angular для модели                                     | компонент 'angular_confirm_dialog'     | да |
| angular_view_model_list_page  | Страница Angular для отображения компонента представления списка                      |                                       |       |
| ionic_angular_view_model_form       | Компонент формы представления Ionic Angular для модели                                      | компонент angular_generic_model_selector | да |
| ionic_angular_view_model_card       | Компонент карточного представления Ionic Angular для модели                                 |                                       | да |
| ionic_angular_view_model_list       | Компонент списка представления Ionic Angular для модели                                     | компонент 'angular_confirm_dialog'     | да |
| ionic_angular_view_model_list_page  | Страница Ionic Angular для отображения компонента представления списка                      |                                       |       |

#### Шаблон php

| name                      | details                                                                   | extra files                           | AI ?  |
| ------------------------- | ------------------------------------------------------------------------- |-------------------------------------- | ----- |
| model                     | Модель класса                                                             |                                       |       |
| pdo_dao                   | Класс DAO (репозиторий) с PDO                                             |                                       |       |
| doctrine_entity           | Doctrine Entity для пользователей Symfony (php 8)                         |                                       |       |
| doctrine_repository       | Репозиторий Doctrine для пользователей Symfony (php 8)                    |                                       |       |
| artisan_make_model        | Создание единственного файла bash с командами artisan make                |                                       |       |
| eloquent_model            | Файл модели Eloquent для пользователей Laravel                            |                                       |       |
| symfony_service           | Сервис Symfony                                                            |                                       |       |
| symfony_api_controller    | Контроллер Symfony                                                        |                                       |       |
| postman                   | Коллекция Postman (будет в корневом каталоге)                             |                                       |       |

#### Шаблон java

| name                      | details                                                                   | extra files                           | AI ?  |
| ------------------------- | ------------------------------------------------------------------------- |-------------------------------------- | ----- |
| model                     | Модель класса                                                             |                                       |       |
| jpa_entity                | Spring JPA Entity                                                         |                                       |       |
| jpa_repository            | Spring JPA Repository                                                     |                                       | да    |
| spring_service            | Сервис Spring с использованием репозитория                                |                                       | да    |
| spring_controller         | Контроллер Spring с использованием сервиса                                |                                       | да    |
| postman                   | Коллекция Postman (будет в корневом каталоге)                             |                                       |       |

#### Шаблон Python

| Название                  | Подробности                                                               | Дополнительные файлы                  | AI ?  |
| ------------------------- | ------------------------------------------------------------------------- | ------------------------------------- | ----- |
| django_model              | Класс модели Django                                                       |                                       |       |
| django_import_models      | Файл `models.py` для импорта ваших моделей, будет в корне                 |                                       |       |
| django_controller         | Класс контроллера Django                                                  |                                       |       |
| django_controller_path    | Шаблоны URL контроллера Django                                            |                                       |       |
| postman                   | Коллекция Postman (будет в корневом каталоге)                             |                                       |       |

#### шаблон csharp

| Название                  | Подробности                                                               | Дополнительные файлы                  | AI ?  |
| ------------------------- | ------------------------------------------------------------------------- |-------------------------------------- | ----- |
| model                     | класс модели                                                              |                                       |       |
| efcore_model              | класс модели EF Core (предпочтительнее использовать efcore для генерации моделей) |                               |       |
| efcore_repository         | репозиторий EF Core                                                       | IAbstractRepository/AbstractRepository|       |
| net_service               | сервис .Net                                                               |                                       |       |
| net_controller            | класс контроллера .Net                                                    |                                       |       |
| net_program               | файл .Net Program.cs с импортом для сервисов/репозиториев                 |                                       |       |
| postman                   | коллекция Postman (будет в корне)                                         |                                       |       |

# Пользовательский базовый шаблон
***custom_base_template*** — это абсолютный путь к вашему каталогу пользовательских шаблонов, если вам нужно что-то особенное.

> Вы не должны использовать это, если в этом нет необходимости.

**Процессоры используют файлы шаблонов для заполнения данных**. Каждый шаблон получает входящие данные, ограниченные **{{ }}**. Если вы используете свой собственный, вы должны знать, что он будет получать.

Вы можете проверить шаблоны в папке `templates` в репозитории GitLab.

**Пример**:

Предположим, вы хотите создать свой собственный файл шаблона для шаблона `java` и процессора `model`. Сначала создайте локальную папку для хранения файла шаблона. Например: "c:/my_templates". Затем перейдите в [папку `templates` репозитория проекта GitLab](https://gitlab.com/codigo-pub/vscode-ext/codigo-generator/-/tree/master/templates?ref_type=heads), затем в папку `java`.

Там вы найдете два файла, связанные с процессором `model` (может быть один или несколько):

- tpl_model_enum.java
- tpl_model.java

Скачайте эти два файла в вашу папку "c:/my_templates", но **не переименовывайте их**!

Измените их содержимое в соответствии с вашими потребностями. Будьте внимательны с данными **{{ }}**.

Теперь вы готовы к работе. Вы можете запустить свой процессор, и он будет использовать ваши файлы.

# Дополнительная конфигурация
*Дополнительная конфигурация*, как уже упоминалось, может использоваться только с некоторыми процессорами.

Пример с дополнительной конфигурацией:

```JSON
    "processors": [
        { "name": "model", "generate": false },
        { "name": "jpa_entity", "extra_config": { "lombok": false } },
        { "name": "jpa_repository", "generate": false }
    ]
```

Это список процессоров и их доступной дополнительной конфигурации:

| шаблон         | имя                       | свойство_extra_config    | значение по умолчанию | Детали                                    | Пример                                                |
| -------------- | ------------------------- | ------------------------ | --------------------- | ----------------------------------------- | ----------------------------------------------------- |
| java           | model                     | lombok                   | true                  | Добавить аннотации lombok                 | "extra_config": { "lombok": false }                   |
| java           | jpa_entity                | lombok                   | true                  | Добавить аннотации lombok                 | "extra_config": { "lombok": false }                   |
| java           | jpa_entity                | use_enum                 | true                  | Использовать класс enum, когда enum       | "extra_config": { "use_enum": false }                 |
| java           | spring_service            | logger                   | true                  | Добавить логгер                           | "extra_config": { "logger": false }                   |
| java           | spring_controller         | logger                   | true                  | Добавить логгер                           | "extra_config": { "logger": false }                   |
| typescript     | loopback_repository       | datasource_name          | _schema name_         | Название источника данных для репозитория | "extra_config": { "datasource_name": "MyApp" }        |
| php            | artisan_make_model        | option                   | *empty string*        | Опция команды модели Artisan              | "extra_config": { "option": "--all" }                 |
| php            | eloquent_model            | do_getters_setters       | false                 | Добавить геттеры/сеттеры                  | "extra_config": { "do_getters_setters": true }        |
| php            | symfony_api_controller    | base_api                 | "/api"                | База для URL API                          | "extra_config": { "base_api": "/myapi" }              |
| php            | symfony_api_controller    | folder_api               | "Api"                 | Название папки для размещения файлов      | "extra_config": { "folder_api": "MyApi" }             |
| java   | spring_security_jwt | user_field/pass_field/role_field  |     | Вы должны назвать поля, используемые для идентификации пользователя (email, username, ..), пароля (pass, password, ...) и роли (role_name, role, ...) | "extra_config": { "user_field": "users.email", "pass_field": "users.password", "role_field": "roles.role" } |

## postman processors

Это свойства `extra_config`, доступные для процессоров `postman`.
**with_base_api** позволяет вам определить, есть ли у вашего API основной базовый путь. Например, `/api`. Как лучшая практика, это будет добавлено как переменная окружения `{{base_api}}` в вашу коллекцию Postman.

``` "extra_config": { "with_base_api": false } ```

**name** позволяет вам задать имя файла Postman, генерируемого процессором. По умолчанию это `default-postman`. Для Java это может включать имя пакета.

``` "extra_config": { "name": "myproject-postman" } ```

## use_model_for_api_endpoint
**use_model_for_api_endpoint** является свойством `extra_config`, доступным для процессоров, которые создают файлы с путями API (например, postman, контроллеры, Angular-сервисы и т. д.).
По умолчанию значение — `false`.
Поведение:

 - true : основной путь модели использует имя таблицы. Например: /users
 - false : основной путь модели использует имя модели. Например: /user

## standalone для Angular
**standalone** является свойством `extra_config`, доступным для процессоров Angular, чтобы запросить создание независимых компонентов. По умолчанию это значение `true`.

# Настройки расширения

На данный момент нет настроек расширения.


---


# Автор

Marc Flausino

# Участники
